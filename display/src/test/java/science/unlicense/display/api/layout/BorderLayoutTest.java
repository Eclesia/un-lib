
package science.unlicense.display.api.layout;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class BorderLayoutTest {

    @Test
    public void testPlacement() {

        final Space butTop = new Space(new Extents(2, 2),BorderConstraint.TOP);
        final Space butBottom = new Space(new Extents(2, 2),BorderConstraint.BOTTOM);
        final Space butLeft = new Space(new Extents(2, 2),BorderConstraint.LEFT);
        final Space butRight = new Space(new Extents(2, 2),BorderConstraint.RIGHT);
        final Space butCenter = new Space(new Extents(2, 2),BorderConstraint.CENTER);

        final Sequence elements = new ArraySequence(new Object[]{butTop,butBottom,butLeft,butRight,butCenter});
        final BorderLayout layout = new BorderLayout(elements);

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(500, 200)));
        layout.update();
        Assert.assertEquals(new Extent.Double(500d, 2d),butTop.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 250,
                0, 1, 1,
                0, 0, 1),
                butTop.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(500d, 2d),butBottom.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 250,
                0, 1, 199,
                0, 0, 1),
                butBottom.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2d, 196d),butLeft.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 1,
                0, 1, 100,
                0, 0, 1),
                butLeft.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2d, 196d),butRight.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 499,
                0, 1, 100,
                0, 0, 1),
                butRight.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(496d, 196d),butCenter.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 250,
                0, 1, 100,
                0, 0, 1),
                butCenter.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(6d, 6d),layout.getExtents(null, new Extent.Double(500,200)).getBest(null));


        //change size ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 300)));
        layout.update();

        Assert.assertEquals(new Extent.Double(100d, 2d),butTop.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 1,
                0, 0, 1),
                butTop.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(100d, 2d),butBottom.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 299,
                0, 0, 1),
                butBottom.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2d, 296d),butLeft.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 1,
                0, 1, 150,
                0, 0, 1),
                butLeft.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2d, 296d),butRight.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 99,
                0, 1, 150,
                0, 0, 1),
                butRight.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(96d, 296d),butCenter.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 150,
                0, 0, 1),
                butCenter.getNodeTransform().viewMatrix());


        //check best size
        Assert.assertEquals(new Extent.Double(6d, 6d),layout.getExtents(null, new Extent.Double(100, 300)).getBest(null));
    }

    @Test
    public void testPlacementOffset() {

        final Space butTop = new Space(new Extents(2, 2),BorderConstraint.TOP);
        final Space butBottom = new Space(new Extents(2, 2),BorderConstraint.BOTTOM);
        final Space butLeft = new Space(new Extents(2, 2),BorderConstraint.LEFT);
        final Space butRight = new Space(new Extents(2, 2),BorderConstraint.RIGHT);
        final Space butCenter = new Space(new Extents(2, 2),BorderConstraint.CENTER);

        final Sequence elements = new ArraySequence(new Object[]{butTop,butBottom,butLeft,butRight,butCenter});
        final BorderLayout layout = new BorderLayout(elements);

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(4, 7), new Vector2f64(504, 207)));
        layout.update();
        Assert.assertEquals(new Extent.Double(500d, 2d),butTop.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 254,
                0, 1, 8,
                0, 0, 1),
                butTop.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(500d, 2d),butBottom.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 254,
                0, 1, 206,
                0, 0, 1),
                butBottom.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2d, 196d),butLeft.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 5,
                0, 1, 107,
                0, 0, 1),
                butLeft.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2d, 196d),butRight.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 503,
                0, 1, 107,
                0, 0, 1),
                butRight.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(496d, 196d),butCenter.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 254,
                0, 1, 107,
                0, 0, 1),
                butCenter.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(6d, 6d),layout.getExtents(null, null).getBest(null));

    }

    /**
     * Check the layout respect the minimum size
     */
    @Test
    public void testMinSize() {

        final Space child = new Space();
        final Extents exts = child.getOverrideExtents();
        exts.minX=10;   exts.minY=15;
        exts.bestX=20;  exts.bestY=30;
        exts.maxX=40;   exts.maxY=45;
        child.setOverrideExtents(exts);
        child.setLayoutConstraint(BorderConstraint.TOP);

        final BorderLayout layout = new BorderLayout(new ArraySequence(new Object[]{child}));
        Assert.assertEquals(new Extent.Double(10, 15), layout.getExtents(null, null).getMin(null));
        Assert.assertEquals(new Extent.Double(20, 30), layout.getExtents(null, null).getBest(null));
        Assert.assertEquals(new Extent.Double(40, 45), layout.getExtents(null, null).getMax(null));


    }

}
