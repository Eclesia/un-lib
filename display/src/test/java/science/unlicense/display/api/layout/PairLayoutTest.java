
package science.unlicense.display.api.layout;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class PairLayoutTest {

    @Test
    public void testAbove() {

        final Space but1 = new Space(new Extent.Double(10, 10));
        final Space but2 = new Space(new Extent.Double(10, 20));
        but1.setOverrideExtents(new Extents(10, 10));
        but2.setOverrideExtents(new Extents(10, 20));

        final PairLayout layout = new PairLayout();
        layout.setDistance(0);
        layout.setRelativePosition(PairLayout.RELATIVE_TOP);
        layout.setHorizontalAlignement(PairLayout.HALIGN_CENTER);
        layout.setVerticalAlignement(PairLayout.VALIGN_CENTER);
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2}));


        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(10, 10),but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 60, // 100/2 + (30/2 * 20/30)
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(10, 20),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 45, // 100/2 - (30/2 * 10/30)
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(10d, 30d),layout.getExtents(null, null).getBest(null));

        //change size ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(20, 30)));
        layout.update();
        Assert.assertEquals(new Extent.Double(10, 10d), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 10,
                0, 1, 25,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(10, 20),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 10,
                0, 1, 10,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(10d, 30d),layout.getExtents(null, null).getBest(null));

    }

    @Test
    public void testGraphicMinSize() {

        final Space but1 = new Space(new Extents(1, 1, 10, 10, 10, 10));
        final Space but2 = new Space(new Extents(10, 1, 10, 10, 10, 10));

        final PairLayout layout = new PairLayout();
        layout.setDistance(0);
        layout.setRelativePosition(PairLayout.RELATIVE_TOP);
        layout.setHorizontalAlignement(PairLayout.HALIGN_CENTER);
        layout.setVerticalAlignement(PairLayout.VALIGN_CENTER);
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2}));


        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(5, 10)));
        layout.update();
        Assert.assertEquals(new Extent.Double(5, 5), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 2.5,
                0, 1, 7.5,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(10, 5),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 2.5,
                0, 1, 2.5,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(10d, 20d),layout.getExtents(null, null).getBest(null));
    }

    @Test
    public void testGraphicMaxSize() {

        final Space but1 = new Space(new Extents(10, 10, 10, 10, 100, 100));
        final Space but2 = new Space(new Extents(10, 10, 10, 10, 100, 100));

        final PairLayout layout = new PairLayout();
        layout.setDistance(0);
        layout.setRelativePosition(PairLayout.RELATIVE_TOP);
        layout.setHorizontalAlignement(PairLayout.HALIGN_CENTER);
        layout.setVerticalAlignement(PairLayout.VALIGN_CENTER);
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2}));


        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 200)));
        layout.update();
        Assert.assertEquals(new Extent.Double(100, 100), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 150,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(100, 100),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 50,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(10d, 20d),layout.getExtents(null, null).getBest(null));
    }

    @Test
    public void testGraphicSingleMaxSize() {

        final Space but1 = new Space(new Extent.Double(10, 10));
        final Extents exts1 = but1.getOverrideExtents();
        exts1.maxX=20;exts1.maxY=30;
        but1.setOverrideExtents(exts1);
        final Space but2 = new Space(new Extent.Double(10, 10));
        final Extents exts2 = but1.getOverrideExtents();
        exts2.maxX=40;exts2.maxY=100;
        but2.setOverrideExtents(exts2);

        final PairLayout layout = new PairLayout();
        layout.setDistance(0);
        layout.setRelativePosition(PairLayout.RELATIVE_TOP);
        layout.setHorizontalAlignement(PairLayout.HALIGN_CENTER);
        layout.setVerticalAlignement(PairLayout.VALIGN_CENTER);
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2}));


        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 200)));
        layout.update();
        Assert.assertEquals(new Extent.Double(20, 30), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 150,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(40, 100),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 85,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(10d, 20d),layout.getExtents(null, null).getBest(null));
    }

    @Test
    public void testHAlign() {

        final Space but1 = new Space(new Extent.Double(10, 10));
        final Space but2 = new Space(new Extent.Double(10, 10));
        but1.setOverrideExtents(new Extents(10, 10));
        but2.setOverrideExtents(new Extents(10, 10));

        final PairLayout layout = new PairLayout();
        layout.setDistance(0);
        layout.setRelativePosition(PairLayout.RELATIVE_LEFT);
        layout.setHorizontalAlignement(PairLayout.HALIGN_LEFT);
        layout.setVerticalAlignement(PairLayout.VALIGN_CENTER);
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2}));


        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 10)));
        layout.update();
        Assert.assertEquals(new Extent.Double(10, 10),but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 15,
                0, 1, 5,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(10, 10),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 5,
                0, 1, 5,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(20d, 10d),layout.getExtents(null, null).getBest(null));

    }

}
