

package science.unlicense.display.api.layout;

import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public final class Space extends AbstractPositionableNode{

    private final Extents extents;

    public Space() {
        this(new Extent.Double(1, 1));
    }

    public Space(Extents extents) {
        this(extents, null);
    }

    public Space(Extent extent) {
        this(new Extents(extent),null);
    }

    public Space(Extents extents, LayoutConstraint constraint) {
        super(false);
        this.extents = extents;
        setLayoutConstraint(constraint);
    }

    @Override
    protected void getExtentsInternal(Extents buffer, Extent constraint) {
        buffer.set(extents);
        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }

}
