
package science.unlicense.display.api.layout;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.SimilarityNd;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class FormLayoutTest {

    @Test
    public void testEmpty() {

        final FormLayout layout = new FormLayout();

        final Similarity transform = SimilarityNd.create(2);
        final Extent size = new Extent.Double(2);

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 100)));

        //check best size
        Assert.assertEquals(new Extent.Double(0d, 0d),layout.getExtents(null, null).getBest(null));

        //change size ----------------------------------------------------------
        //should have no effect
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(200, 200)));

        //check best size
        Assert.assertEquals(new Extent.Double(0d, 0d),layout.getExtents(null, null).getBest(null));

    }

    @Test
    public void testOneWidget() {

        final Space space1 = new Space(new Extents(30, 10),FillConstraint.builder().build());

        final FormLayout layout = new FormLayout(new ArraySequence(new Object[]{space1}));

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(30, 10),space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 15,
                0, 1, 5,
                0, 0, 1),
                space1.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(30d, 10d),layout.getExtents(null, null).getBest(null));

        //change size ----------------------------------------------------------
        //should have no effect
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(200, 200)));
        layout.update();
        Assert.assertEquals(new Extent.Double(30, 10), space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 15,
                0, 1, 5,
                0, 0, 1),
                space1.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(30d, 10d),layout.getExtents(null, null).getBest(null));

    }

    @Test
    public void testTreeWidgetOneRow() {

        final Space space1 = new Space(new Extents(5, 10),FillConstraint.builder().coord(0, 0).fill(true, true).build());
        final Space space2 = new Space(new Extents(15, 5),FillConstraint.builder().coord(1, 0).fill(true, true).build());
        final Space space3 = new Space(new Extents(30, 2),FillConstraint.builder().coord(2, 0).fill(true, true).build());

        final FormLayout layout = new FormLayout(new ArraySequence(new Object[]{space1,space2,space3}));
        layout.setDefaultColumnSpace(3);
        layout.setDefaultRowSpace(3);

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(5, 10),space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 2.5,
                0, 1, 5,
                0, 0, 1),
                space1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(15, 10),space2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 8+7.5, // 5 +3
                0, 1, 5,
                0, 0, 1),
                space2.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(30, 10),space3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 26+15, // 5 + 3 + 15 + 3
                0, 1, 5,
                0, 0, 1),
                space3.getNodeTransform().viewMatrix());

        //check best size
        // 5 + 3 + 15 + 3 + 30
        Assert.assertEquals(new Extent.Double(56, 10),layout.getExtents(null, null).getBest(null));

    }

    @Test
    public void testVerticalExpand() {

        final Space space1 = new Space(new Extents(10, 10), FillConstraint.builder().coord(0, 0).build());
        final Space space3 = new Space(new Extents(10, 10), FillConstraint.builder().coord(0, 2).build());

        final FormLayout layout = new FormLayout(new ArraySequence(new Object[]{space1,space3}));
        layout.setRowSize(1, FormLayout.SIZE_EXPAND);
        layout.setDefaultColumnSpace(3);
        layout.setDefaultRowSpace(3);


        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(10, 10),space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 5,
                0, 1, 5,
                0, 0, 1),
                space1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(10, 10),space3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 5,
                0, 1, 95,
                0, 0, 1),
                space3.getNodeTransform().viewMatrix());

        //check best size
        // 10 + 10 + 2*3 (row spacing)
        Assert.assertEquals(new Extent.Double(10, 26),layout.getExtents(null, null).getBest(null));
    }

    @Test
    public void testSpanY() {

        final Space space0 = new Space(new Extents(20, 100), FillConstraint.builder().coord(0, 0).span(1, 2).fill(true, true).build());
        final Space space1 = new Space(new Extents(10, 10), FillConstraint.builder().coord(1, 0).fill(true, true).build());
        final Space space3 = new Space(new Extents(10, 10), FillConstraint.builder().coord(1, 1).fill(true, true).build());

        final FormLayout layout = new FormLayout(new ArraySequence(new Object[]{space0,space1,space3}));
        layout.setDefaultColumnSpace(0);
        layout.setDefaultRowSpace(0);

        Assert.assertEquals(new Extent.Double(30, 100),layout.getExtents(null, null).getBest(null));

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(10, 50),space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 10+15,
                0, 1, 25,
                0, 0, 1),
                space1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(10, 50),space3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 20+5,
                0, 1, 50+25,
                0, 0, 1),
                space3.getNodeTransform().viewMatrix());

    }

    @Test
    public void testTreeReserveSpace() {

        final Space space1 = new Space(new Extents(5, 10),FillConstraint.builder().coord(0, 0).fill(true, true).build());
        final Space space2 = new Space(new Extents(15, 5),FillConstraint.builder().coord(1, 0).fill(true, true).build());

        final FormLayout layout = new FormLayout(new ArraySequence(new Object[]{space1,space2}));
        layout.setColumnSize(0, FormLayout.SIZE_EXPAND);

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(85, 10),space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 42.5,
                0, 1, 5,
                0, 0, 1),
                space1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(15, 10),space2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 92.5,
                0, 1, 5,
                0, 0, 1),
                space2.getNodeTransform().viewMatrix());

        Assert.assertEquals(new Extent.Double(20, 10),layout.getExtents(null, null).getBest(null));

        //set reserve space and visibility to false
        space2.setVisible(false);
        space2.setReserveSpace(false);
        layout.update();
        Assert.assertEquals(new Extent.Double(100, 10),space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 5,
                0, 0, 1),
                space1.getNodeTransform().viewMatrix());
        Assert.assertEquals(false, space2.getNodeTransform().getScale().isFinite());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 92.5,
                0, 1, 5,
                0, 0, 1),
                space2.getNodeTransform().viewMatrix());

        Assert.assertEquals(new Extent.Double(5, 10),layout.getExtents(null, null).getBest(null));


    }

}
