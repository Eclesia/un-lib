
package science.unlicense.display.api.painter2d;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public abstract class Painter2DTest {

    protected abstract Painter2D createPainter(int width,int height);

    @Test
    public void testClipProperty() {

        final Painter2D painter = createPainter(100, 100);
        Assert.assertNull(painter.getClip());

        //test get and set clip without transform
        Rectangle clip = new Rectangle(10, 20, 30, 40);
        painter.setClip(clip);
        Assert.assertEquals(clip,painter.getClip());

        //test getting clip with a transform
        painter.setTransform(new Affine2(2, 0, 5, 0, 1, -5));
        Assert.assertEquals(clip,painter.getClip());

        clip = new Rectangle(1, 2, 3, 4);
        painter.setClip(clip);
        Assert.assertEquals(clip,painter.getClip());

        //create a derivate painter
        Painter2D derivate = painter.derivate(false);
        Assert.assertNull(derivate.getClip());




    }

}
