
package science.unlicense.display.api.scene;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.exception.InvalidArgumentException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.api.Affine;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.SimilarityNd;
import science.unlicense.math.impl.Vector3f64;

/**
 *
 * @author Johann Sorel
 */
public class SceneNodeTest {

    private static final double DELTA = 0.0000001;

    /**
     * Check raised exception between node dimension sizes.
     */
    @Test
    public void testNodeDimension() {

        final SceneNode parent = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode child = new DefaultSceneNode(CoordinateSystems.UNDEFINED_2D);

        try {
            parent.getChildren().add(child);
            Assert.fail("Different scene node diemnsions should raise an exception.");
        } catch(InvalidArgumentException ex) {
            //ok
        }

    }

    /**
     * When there is no parent, the node transform should be equal to NodeToRootSpace matrix.
     */
    @Test
    public void testNodeToRootTransform() {

        final SceneNode node = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        node.getNodeTransform().getTranslation().setXYZ(2, 1, 3);
        node.getNodeTransform().notifyChanged();

        Assert.assertEquals(node.getNodeTransform().viewMatrix(), node.getNodeToRootSpace().toMatrix());

    }

    @Test
    public void testRootToNodeTransform() {

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final SceneNode n1 = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        n1.getNodeTransform().getTranslation().setXYZ(2, 1, 3);
        n1.getNodeTransform().notifyChanged();
        final SceneNode n2 = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        n2.getNodeTransform().getTranslation().setXYZ(3, -5, 7);
        n2.getNodeTransform().notifyChanged();
        scene.getChildren().add(n1);
        n1.getChildren().add(n2);

        //calculate our reference vector
        final VectorRW reference = new Vector3f64(0, 0, 0);
        n1.getNodeTransform().transform(reference,reference);
        n2.getNodeTransform().transform(reference,reference);

        Assert.assertEquals(5, reference.get(0), DELTA);
        Assert.assertEquals(-4, reference.get(1), DELTA);
        Assert.assertEquals(10, reference.get(2), DELTA);

        final Affine worldToNode = n2.getRootToNodeSpace();
        final VectorRW candidate = new Vector3f64(0, 0, 0);
        worldToNode.transform(candidate,candidate);

        //we should be back to 0
        Assert.assertEquals(new Vector3f64(-5, 4, -10), candidate);

    }

    @Test
    public void testUpdateRootToNodeTransformNoParent() {

        final SceneNode n1 = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        n1.getNodeTransform().getTranslation().setXYZ(1, 2, 3);
        n1.getNodeTransform().notifyChanged();

        n1.setRootToNodeSpace(SimilarityNd.create(new Matrix3x3().setToIdentity(),
                new Vector3f64(1,1,1),
                new Vector3f64(-2,-4,-6)));

        //check the n1 transform has changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 2,
                0, 1, 0, 4,
                0, 0, 1, 6,
                0, 0, 0, 1),
                n1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -2,
                0, 1, 0, -4,
                0, 0, 1, -6,
                0, 0, 0, 1),
                n1.getRootToNodeSpace().toMatrix());

    }

    @Test
    public void testUpdateRootToNodeTransform() {

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        scene.getNodeTransform().getTranslation().setXYZ(50, 60, 70);
        scene.getNodeTransform().notifyChanged();
        final SceneNode n1 = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        n1.getNodeTransform().getTranslation().setXYZ(1, 2, 3);
        n1.getNodeTransform().notifyChanged();
        scene.getChildren().add(n1);

        n1.setRootToNodeSpace(SimilarityNd.create(new Matrix3x3().setToIdentity(),
                new Vector3f64(1,1,1),
                new Vector3f64(-52,-64,-76)));

        //check parent has not changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 50,
                0, 1, 0, 60,
                0, 0, 1, 70,
                0, 0, 0, 1),
                scene.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -50,
                0, 1, 0, -60,
                0, 0, 1, -70,
                0, 0, 0, 1),
                scene.getRootToNodeSpace().toMatrix());

        //check the n1 transform has changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 2,
                0, 1, 0, 4,
                0, 0, 1, 6,
                0, 0, 0, 1),
                n1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -52,
                0, 1, 0, -64,
                0, 0, 1, -76,
                0, 0, 0, 1),
                n1.getRootToNodeSpace().toMatrix());

    }

}
