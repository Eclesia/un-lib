
package science.unlicense.display.api.layout;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class LineLayoutTest {

    @Test
    public void testPlacement() {

        final Space butTop = new Space(new Extents(2, 2),BorderConstraint.TOP);
        final Space butBottom = new Space(new Extents(2, 2),BorderConstraint.BOTTOM);
        final Space butLeft = new Space(new Extents(2, 2),BorderConstraint.LEFT);
        final Space butRight = new Space(new Extents(2, 2),BorderConstraint.RIGHT);
        final Space butCenter = new Space(new Extents(2, 2),BorderConstraint.CENTER);

        final Sequence elements = new ArraySequence(new Object[]{butTop,butBottom,butLeft,butRight,butCenter});
        final LineLayout layout = new LineLayout();
        layout.setPositionables(elements);

        //placement element with enough size available
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(500, 200)));
        layout.update();
        Assert.assertEquals(new Extent.Double(2,2),butTop.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 1,
                0, 1, 1,
                0, 0, 1),
                butTop.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butBottom.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 7,
                0, 1, 1,
                0, 0, 1),
                butBottom.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butLeft.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 13,
                0, 1, 1,
                0, 0, 1),
                butLeft.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butRight.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 19,
                0, 1, 1,
                0, 0, 1),
                butRight.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butCenter.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 1,
                0, 0, 1),
                butCenter.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(26d, 2d),layout.getExtents(null, null).getBest(null));


        //change size , not enough space on x for all elements
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(17, 200)));
        layout.update();
        Assert.assertEquals(new Extent.Double(2,2),butTop.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 1,
                0, 1, 1,
                0, 0, 1),
                butTop.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butBottom.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 7,
                0, 1, 1,
                0, 0, 1),
                butBottom.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butLeft.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 13,
                0, 1, 1,
                0, 0, 1),
                butLeft.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butRight.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 1,
                0, 1, 7,
                0, 0, 1),
                butRight.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butCenter.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 7,
                0, 1, 7,
                0, 0, 1),
                butCenter.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(14d, 8d),layout.getExtents(null, new Extent.Double(17, 200)).getBest(null));
    }

}
