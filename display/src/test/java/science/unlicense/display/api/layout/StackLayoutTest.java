
package science.unlicense.display.api.layout;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class StackLayoutTest {

    @Test
    public void test2() {

        final Space but1 = new Space(new Extent.Double(10, 20));
        final Space but2 = new Space(new Extent.Double(20, 10));
        but1.setLayoutConstraint(new StackConstraint(1));
        but2.setLayoutConstraint(new StackConstraint(2));

        final StackLayout layout = new StackLayout();
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2}));


        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(100, 100),but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 50,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(100, 100),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 50,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(20, 20),layout.getExtents(null, null).getBest(null));

        //change size ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(-20, -40), new Vector2f64(20,40)));
        layout.update();
        Assert.assertEquals(new Extent.Double(40, 80), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 0,
                0, 1, 0,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(40, 80),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 0,
                0, 1, 0,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(20, 20),layout.getExtents(null, null).getBest(null));

    }

}
