
package science.unlicense.display.api.painter2d;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.impl.Vector2i32;

/**
 *
 * @author Johann Sorel
 */
public abstract class StrokeTest {

    private static final Color TRS = new ColorRGB(0, 0, 0, 0);

    protected abstract ImagePainter2D createPainter(int width,int height);

    @Test
    @Ignore
    public void testStrokeLine() {

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 3
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 4
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 5
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 6
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 7
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 8
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 9
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //10
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //11
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //12
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //13
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        final ImagePainter2D painter = createPainter(20, 20);
        final Line line = new DefaultLine(4, 3, 4, 15);
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
        painter.setPaint(new ColorPaint(Color.RED));
        painter.stroke(line);
        painter.flush();


        final Image image = painter.getImage();
        for (int y=0;y<20;y++) {
            for (int x=0;x<20;x++) {
                final Color c = image.getColor(new Vector2i32(x,y));
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertEquals(exp, c, " at coord "+y+" "+x);
            }
        }
    }

}
