
package science.unlicense.display.api.layout;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class BlockLayoutTest {

    @Test
    public void test2x2() {

        final Space but1 = new Space(new Extent.Double(10, 20));
        final Space but2 = new Space(new Extent.Double(10, 20));
        final Space but3 = new Space(new Extent.Double(10, 20));
        final Space but4 = new Space(new Extent.Double(10, 20));

        final BlockLayout layout = new BlockLayout(new Extent.Double(50, 20));
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2,but3,but4}));


        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(50, 20),but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 10,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(50, 20),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 75,
                0, 1, 10,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(50, 20),but3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 30,
                0, 0, 1),
                but3.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(50, 20),but4.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 75,
                0, 1, 30,
                0, 0, 1),
                but4.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(100d, 40d),layout.getExtents(null, null).getBest(null));

        //change size ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(80, 500)));
        layout.update();
        Assert.assertEquals(new Extent.Double(50, 20), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 10,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(50, 20),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 30,
                0, 0, 1),
                but2.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(50, 20),but3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 50,
                0, 0, 1),
                but3.getNodeTransform().viewMatrix());
        Assert.assertEquals(new Extent.Double(50, 20),but4.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 70,
                0, 0, 1),
                but4.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(50d, 80d),layout.getExtents(null, null).getBest(null));

    }

}
