
package science.unlicense.display.api.layout;

import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class DisplacementLayoutTest {

    @Test
    public void testNoOffset() {

        final Space but1 = new Space(new Extent.Double(40, 20));

        final DisplacementLayout layout = new DisplacementLayout();
        layout.setHorizontalAlignement(DisplacementLayout.HALIGN_LEFT);
        layout.setVerticalAlignement(DisplacementLayout.VALIGN_TOP);
        layout.setFillWidth(true);
        layout.setPositionables(new ArraySequence(new Object[]{but1}));


        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(0, 0), new Vector2f64(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(100, 20),but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 0,
                0, 1, -40,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(40, 20),layout.getExtents(null, null).getBest(null));

        //change size ----------------------------------------------------------
        layout.setView(new BBox(new Vector2f64(-40, -40), new Vector2f64(20,40)));
        layout.update();
        Assert.assertEquals(new Extent.Double(60, 20), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 0,
                0, 1, -30,
                0, 0, 1),
                but1.getNodeTransform().viewMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(40, 20),layout.getExtents(null, null).getBest(null));

    }

}
