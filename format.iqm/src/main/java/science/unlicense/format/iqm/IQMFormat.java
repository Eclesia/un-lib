
package science.unlicense.format.iqm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * Inter-Quake Model format.
 *
 * Resources :
 * http://sauerbraten.org/iqm/
 * http://sauerbraten.org/iqm/iqm.txt
 *
 * @author Johann Sorel
 */
public class IQMFormat extends AbstractModel3DFormat{

    public static final IQMFormat INSTANCE = new IQMFormat();

    private IQMFormat() {
        super(new Chars("IQM"));
        shortName = new Chars("IQM");
        longName = new Chars("Inter-Quake Model Format");
        extensions.add(new Chars("iqm"));
        signatures.add(IQMStore.SIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new IQMStore(input);
    }

}
