
package science.unlicense.format.iqm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class IQMMesh {

    public int name;
    public int material;
    public int firstVertex;
    public int numVertex;
    public int firstTriangle;
    public int numTriangle;

    public void read(DataInputStream ds) throws IOException {
        name = ds.readInt();
        material = ds.readInt();
        firstVertex = ds.readInt();
        numVertex = ds.readInt();
        firstTriangle = ds.readInt();
        numTriangle = ds.readInt();
    }

}
