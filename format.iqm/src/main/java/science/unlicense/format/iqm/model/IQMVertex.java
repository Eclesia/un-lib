
package science.unlicense.format.iqm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class IQMVertex {

    public float[] position;
    public float[] texcoord;
    public float[] normal;
    public float[] tangent;
    public byte[] blendindices;
    public byte[] blendweights;
    public byte[] color;

    public void read(DataInputStream ds) throws IOException {
        position = ds.readFloat(3);
        texcoord = ds.readFloat(2);
        normal = ds.readFloat(3);
        tangent = ds.readFloat(4);
        blendindices = ds.readBytes(4);
        blendweights = ds.readBytes(4);
        color = ds.readBytes(4);
    }

}
