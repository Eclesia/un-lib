
package science.unlicense.format.iqm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class IQMExtension {

    public int name;
    public int num_data;
    public int ofs_data;
    public int ofs_extensions;

    public void read(DataInputStream ds) throws IOException {
        name = ds.readInt();
        num_data = ds.readInt();
        ofs_data = ds.readInt();
        ofs_extensions = ds.readInt();
    }

}
