
package science.unlicense.format.iqm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class IQMAnim {

    public static final int IQM_LOOP = 1<<0;

    public int name;
    public int firstFrame;
    public int numFrames;
    public float frameRate;
    public int flags;

    public void read(DataInputStream ds) throws IOException {
        name = ds.readInt();
        firstFrame = ds.readInt();
        numFrames = ds.readInt();
        frameRate = ds.readFloat();
        flags = ds.readInt();
    }

}
