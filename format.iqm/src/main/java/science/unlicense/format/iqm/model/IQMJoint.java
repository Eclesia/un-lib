
package science.unlicense.format.iqm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class IQMJoint {

    public int name;
    public int parent;
    public float[] translate;
    /** quaternion */
    public float[] rotation;
    public float[] scale;

    public void read(DataInputStream ds) throws IOException {
        name = ds.readInt();
        parent = ds.readInt();
        translate = ds.readFloat(3);
        rotation = ds.readFloat(4);
        scale = ds.readFloat(3);

    }

}
