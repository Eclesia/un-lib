
package science.unlicense.format.iqm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class IQMTriangle {

    public int idx0;
    public int idx1;
    public int idx2;

    public void read(DataInputStream ds) throws IOException {
        idx0 = ds.readInt();
        idx1 = ds.readInt();
        idx2 = ds.readInt();
    }

}
