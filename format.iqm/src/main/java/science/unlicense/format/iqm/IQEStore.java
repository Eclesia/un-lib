
package science.unlicense.format.iqm;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.FloatSequence;
import science.unlicense.common.api.collection.primitive.IntSequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import static science.unlicense.format.iqm.IQEReader.*;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * @author Johann Sorel
 */
public class IQEStore extends AbstractModel3DStore {

    public static final byte[] SIGNATURE = new byte[]{'#',' ','I','n','t','e','r','-','Q','u','a','k','e',' ','E','x','p','o','r','t'};

    public IQEStore(Object input) {
        super(IQEFormat.INSTANCE, input);
    }

    @Override
    public Collection getElements() throws StoreException {

        final Sequence col = new ArraySequence();
        final MotionModel mpm = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);
        final Skeleton skeleton = new Skeleton(3);
        mpm.setSkeleton(skeleton);
        col.add(mpm);

        State current = null;
        Joint currentJoint = null;
        final Sequence allJoints = new ArraySequence();
        int jointInc = 0;

        final IQEReader reader = new IQEReader();
        try {
            reader.setInput(source);

            for (IQEReader.Record rec=reader.next(); rec!=null; rec=reader.next()) {

                if (COMMENT.equals(rec.type)) {
                    //skip it
                } else if (VERTEX_POS.equals(rec.type)) {
                    //NOTE ignore w value, what should we do with it ?
                    switch (rec.values.length) {
                        case 0 :
                            current.vertex.put(0);
                            current.vertex.put(0);
                            current.vertex.put(0);
                            break;
                        case 3 :
                        case 4 :
                            current.vertex.put(((Number) rec.values[0]).floatValue());
                            current.vertex.put(((Number) rec.values[1]).floatValue());
                            current.vertex.put(((Number) rec.values[2]).floatValue());
                            break;
                        default : throw new IOException(this, "Unexpected record length");
                    }
                } else if (VERTEX_TEX.equals(rec.type)) {
                    switch (rec.values.length) {
                        case 0 :
                            current.uv.put(0);
                            current.uv.put(0);
                            break;
                        case 2 :
                            current.uv.put(((Number) rec.values[0]).floatValue());
                            current.uv.put(((Number) rec.values[1]).floatValue());
                            break;
                        default : throw new IOException(this, "Unexpected record length");
                    }

                } else if (VERTEX_NOR.equals(rec.type)) {
                    switch (rec.values.length) {
                        case 3 :
                            current.normal.put(((Number) rec.values[0]).floatValue());
                            current.normal.put(((Number) rec.values[1]).floatValue());
                            current.normal.put(((Number) rec.values[2]).floatValue());
                            break;
                        default : throw new IOException(this, "Unexpected record length");
                    }

                } else if (VERTEX_BLE.equals(rec.type)) {
                    switch (rec.values.length) {
                        case 0 :
                            current.jointIndex.put(0);
                            current.jointIndex.put(0);
                            current.jointIndex.put(0);
                            current.jointIndex.put(0);
                            current.jointWeigth.put(0);
                            current.jointWeigth.put(0);
                            current.jointWeigth.put(0);
                            current.jointWeigth.put(0);
                            break;
                        case 2 :
                            current.jointIndex.put(((Number) rec.values[0]).intValue());
                            current.jointIndex.put(0);
                            current.jointIndex.put(0);
                            current.jointIndex.put(0);
                            current.jointWeigth.put(((Number) rec.values[1]).floatValue());
                            current.jointWeigth.put(0);
                            current.jointWeigth.put(0);
                            current.jointWeigth.put(0);
                            break;
                        case 4 :
                            current.jointIndex.put(((Number) rec.values[0]).intValue());
                            current.jointIndex.put(((Number) rec.values[2]).intValue());
                            current.jointIndex.put(0);
                            current.jointIndex.put(0);
                            current.jointWeigth.put(((Number) rec.values[1]).floatValue());
                            current.jointWeigth.put(((Number) rec.values[3]).floatValue());
                            current.jointWeigth.put(0);
                            current.jointWeigth.put(0);
                            break;
                        case 6 :
                            current.jointIndex.put(((Number) rec.values[0]).intValue());
                            current.jointIndex.put(((Number) rec.values[2]).intValue());
                            current.jointIndex.put(((Number) rec.values[4]).intValue());
                            current.jointIndex.put(0);
                            current.jointWeigth.put(((Number) rec.values[1]).floatValue());
                            current.jointWeigth.put(((Number) rec.values[3]).floatValue());
                            current.jointWeigth.put(((Number) rec.values[5]).floatValue());
                            current.jointWeigth.put(0);
                            break;
                        case 8 :
                            current.jointIndex.put(((Number) rec.values[0]).intValue());
                            current.jointIndex.put(((Number) rec.values[2]).intValue());
                            current.jointIndex.put(((Number) rec.values[4]).intValue());
                            current.jointIndex.put(((Number) rec.values[6]).intValue());
                            current.jointWeigth.put(((Number) rec.values[1]).floatValue());
                            current.jointWeigth.put(((Number) rec.values[3]).floatValue());;
                            current.jointWeigth.put(((Number) rec.values[5]).floatValue());
                            current.jointWeigth.put(((Number) rec.values[7]).floatValue());
                            break;
                        default : throw new IOException(this, "Unexpected record length");
                    }

                } else if (VERTEX_COL.equals(rec.type)) {
                    switch (rec.values.length) {
                        case 0 :
                            current.color.put(0);
                            current.color.put(0);
                            current.color.put(0);
                            current.color.put(1);
                            break;
                        case 3 :
                            current.color.put(((Number) rec.values[0]).floatValue());
                            current.color.put(((Number) rec.values[1]).floatValue());
                            current.color.put(((Number) rec.values[2]).floatValue());
                            current.color.put(1);
                            break;
                        case 4 :
                            current.color.put(((Number) rec.values[0]).floatValue());
                            current.color.put(((Number) rec.values[1]).floatValue());
                            current.color.put(((Number) rec.values[2]).floatValue());
                            current.color.put(((Number) rec.values[3]).floatValue());
                            break;
                        default : throw new IOException(this, "Unexpected record length");
                    }
                } else if (VERTEX_CUS.equals(rec.type)) {

                } else if (VERTEX_ARRAY.equals(rec.type)) {

                } else if (MESH.equals(rec.type)) {
                    if (current != null) mpm.getChildren().add(current.finish());
                    current = new State();
                    current.mesh.getSkin().setSkeleton(skeleton);
                } else if (MATERIAL.equals(rec.type)) {

                    final Chars textureName = (Chars) rec.values[0];


                } else if (TRIANGLE_FROM_START.equals(rec.type)) {

                } else if (TRIANGLE_FROM_MESH.equals(rec.type)) {

                    current.triangles.put(((Number) rec.values[0]).intValue());
                    current.triangles.put(((Number) rec.values[1]).intValue());
                    current.triangles.put(((Number) rec.values[2]).intValue());

                } else if (SMOOTH_UV.equals(rec.type)) {

                } else if (SMOOTH_GROUP.equals(rec.type)) {

                } else if (SMOOTH_ANGLE.equals(rec.type)) {

                } else if (SMOOTH_VERTEX.equals(rec.type)) {

                } else if (SMOOTH_TRIANGLE.equals(rec.type)) {

                } else if (POSE_QUATERNION.equals(rec.type)) {

                } else if (POSE_MATRIX.equals(rec.type)) {

                } else if (POSE_ANGLE.equals(rec.type)) {

                } else if (JOINT.equals(rec.type)) {
                    currentJoint = new Joint(3);
                    currentJoint.setId(jointInc);
                    currentJoint.setTitle((CharArray) rec.values[0]);
                    allJoints.add(currentJoint);
                    jointInc++;

                    if (rec.values.length>1) {
                        //has a parent
                        int parentIndex = ((Number) rec.values[1]).intValue();
                        if (parentIndex>0) {
                            Joint parent = (Joint) allJoints.get(parentIndex);
                            parent.getChildren().add(currentJoint);
                        } else {
                            skeleton.getChildren().add(currentJoint);
                        }
                    } else {
                        skeleton.getChildren().add(currentJoint);
                    }


                } else if (ANIMATION.equals(rec.type)) {

                } else if (LOOP.equals(rec.type)) {

                } else if (FRAMERATE.equals(rec.type)) {

                } else if (FRAME.equals(rec.type)) {

                } else {
                    throw new IOException(this, "Unexpected line : "+rec.type);
                }
            }
        } catch (IOException ex) {
            throw new StoreException(ex);
        } finally {
            try {
                reader.dispose();
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }

        //finish last mesh
        if (current != null) mpm.getChildren().add(current.finish());

        return col;
    }


    private static class State {

        final DefaultModel mesh = new DefaultModel();
        final FloatSequence vertex = new FloatSequence();
        final FloatSequence normal = new FloatSequence();
        final FloatSequence uv = new FloatSequence();
        final FloatSequence color = new FloatSequence();
        //we store up to 4 indexes
        final IntSequence jointIndex = new IntSequence();
        final FloatSequence jointWeigth = new FloatSequence();
        final IntSequence triangles = new IntSequence();

        public State() {
            final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
            mesh.setSkin(new Skin());
            final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
            material.setDiffuse(Color.BLUE);
            mesh.getMaterials().add(material);
            mesh.getSkin().setMaxWeightPerVertex(4);
            mesh.setShape(shell);
        }

        public DefaultModel finish() {
            final DefaultMesh shell = (DefaultMesh) mesh.getShape();
            shell.setPositions(InterleavedTupleGrid1D.create(vertex.toArrayFloat(), 3));
            shell.setNormals(InterleavedTupleGrid1D.create(normal.toArrayFloat(), 3));
            shell.setUVs(InterleavedTupleGrid1D.create(uv.toArrayFloat(), 2));
            shell.getAttributes().add(Skin.ATT_JOINTS_0, InterleavedTupleGrid1D.create(jointIndex.toArrayInt(), 4));
            shell.getAttributes().add(Skin.ATT_WEIGHTS_0, InterleavedTupleGrid1D.create(jointWeigth.toArrayFloat(), 4));
            shell.setIndex(InterleavedTupleGrid1D.create(triangles.toArrayInt(),1));
            shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, triangles.getSize())});
            shell.getBoundingBox();
            return mesh;
        }
    }

}
