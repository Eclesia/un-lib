
package science.unlicense.format.iqm;

import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.format.iqm.model.IQMFile;

/**
 *
 * @author Johann Sorel
 */
public class IQMReader extends AbstractReader {

    public IQMFile read() throws IOException {
        final BacktrackInputStream bs = getInputAsBacktrackStream();

        IQMFile file = new IQMFile();
        file.read(bs);
        return file;
    }

}
