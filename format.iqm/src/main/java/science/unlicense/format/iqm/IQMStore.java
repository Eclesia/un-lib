
package science.unlicense.format.iqm;

import science.unlicense.common.api.buffer.Buffer;
import science.unlicense.common.api.buffer.DefaultFloat32Buffer;
import science.unlicense.common.api.buffer.DefaultFloat64Buffer;
import science.unlicense.common.api.buffer.DefaultInt16Buffer;
import science.unlicense.common.api.buffer.DefaultInt32Buffer;
import science.unlicense.common.api.buffer.DefaultInt8Buffer;
import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.FormatEncodingException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.iqm.model.IQMFile;
import science.unlicense.format.iqm.model.IQMVertexArray;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.material.TextureMapping;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * @author Johann Sorel
 */
public class IQMStore extends AbstractModel3DStore {

    public static final byte[] SIGNATURE = new byte[]{'I','N','T','E','R','Q','U','A','K','E','M','O','D','E','L',0x00};
    public static final Chars MD5_VERSION = Chars.constant("MD5Version");

    public IQMStore(Object input) {
        super(IQMFormat.INSTANCE, input);
    }

    @Override
    public Collection getElements() throws StoreException {

        final Sequence col = new ArraySequence();
        final IQMReader reader = new IQMReader();
        try {
            reader.setInput(source);
            final IQMFile file = reader.read();
            transform(file, col);
        } catch (IOException ex) {
            try {
                reader.dispose();
            } catch (IOException ex1) {
                throw new StoreException(ex1);
            }
            throw new StoreException(ex);
        }

        return col;
    }

    private void transform(IQMFile file, Sequence col) throws IOException {

        final MotionModel mpm = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);

        final IBO ibo = new IBO(file.triangles);

        VBO vertices = null;
        VBO normals = null;
        VBO tangents = null;
        VBO uvs = null;
        VBO color = null;

        for (int i=0; i<file.vertexArays.length; i++) {
            final Object data = file.vertexArays[i].data;
            final Buffer buffer;
            switch (file.vertexArays[i].format) {
                case IQMVertexArray.FORMAT_BYTE   : buffer = new DefaultInt8Buffer((byte[]) data); break;
                case IQMVertexArray.FORMAT_UBYTE  : buffer = new DefaultInt32Buffer((int[]) data); break;
                case IQMVertexArray.FORMAT_SHORT  : buffer = new DefaultInt16Buffer((short[]) data); break;
                case IQMVertexArray.FORMAT_USHORT : buffer = new DefaultInt32Buffer((int[]) data); break;
                case IQMVertexArray.FORMAT_INT    : buffer = new DefaultInt32Buffer((int[]) data); break;
                case IQMVertexArray.FORMAT_UINT   : buffer = new DefaultInt32Buffer((int[]) data); break;
                case IQMVertexArray.FORMAT_HALF   : throw new FormatEncodingException(this, "Unsupported vertex array format " + file.vertexArays[i].format);
                case IQMVertexArray.FORMAT_FLOAT  : buffer = new DefaultFloat32Buffer((float[]) data); break;
                case IQMVertexArray.FORMAT_DOUBLE : buffer = new DefaultFloat64Buffer((double[]) data); break;
                default : throw new FormatEncodingException(this, "Unexpected vertex array format " + file.vertexArays[i].format);
            }

            final VBO vbo = new VBO(buffer, file.vertexArays[i].size);
            switch (file.vertexArays[i].type) {
                case IQMVertexArray.TYPE_POSITION : vertices = vbo; break;
                case IQMVertexArray.TYPE_TEXCOORD : uvs = vbo; break;
                case IQMVertexArray.TYPE_NORMAL   : normals = vbo; break;
                case IQMVertexArray.TYPE_TANGENT  : tangents = vbo; break;
                case IQMVertexArray.TYPE_COLOR : color = vbo; break;
                case IQMVertexArray.TYPE_BLENDINDEXES :
                case IQMVertexArray.TYPE_BLENDWEIGHTS :
                case IQMVertexArray.TYPE_CUSTOM  :
            }
        }

        for (int i=0;i<file.meshes.length; i++) {
            final DefaultModel mesh = new DefaultModel();
            mesh.getTechniques().add(new SimpleBlinnPhong());

            final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
            shell.setPositions(vertices);
            shell.setNormals(normals);
            shell.setTangents(tangents);
            shell.setUVs(uvs);
            shell.setIndex(ibo);
            shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(file.meshes[i].firstTriangle*3, file.meshes[i].numTriangle*3)});
            Mesh.calculateNormals(shell);

            final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
            material.setDiffuse(Color.WHITE);

            mesh.setId(file.texts.getValue(file.meshes[i].name));
            mesh.setTitle((CharArray) file.texts.getValue(file.meshes[i].name));
            mesh.getMaterials().add(material);
            mesh.setShape(shell);

            final Chars materialName = (Chars) file.texts.getValue(file.meshes[i].material);
            if (!materialName.isEmpty()) {
                final Path pi = ((Path) source);
                final Image image = Images.read(pi.getParent().resolve(materialName));
                material.setDiffuseTexture(new TextureMapping(new Texture2D(image)));
            }

            mesh.setShape(shell);
            mpm.getChildren().add(mesh);
        }

        col.add(mpm);
    }

}
