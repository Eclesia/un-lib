
package science.unlicense.format.iqm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class IQMPose {

    public int parent;
    public int channelmask;
    public float[] channeloffset;
    public float[] channelscale;

    public void read(DataInputStream ds) throws IOException {
        parent = ds.readInt();
        channelmask = ds.readInt();
        channeloffset = ds.readFloat(10);
        channelscale = ds.readFloat(10);

    }

}
