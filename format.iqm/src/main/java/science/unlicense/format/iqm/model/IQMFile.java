
package science.unlicense.format.iqm.model;

import java.util.Arrays;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.FormatEncodingException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.format.iqm.IQMStore;

/**
 *
 * @author Johann Sorel
 */
public class IQMFile {

    public int version;
    public int fileSize;
    public int flags;
    public int num_text, ofs_text;
    public int num_meshes, ofs_meshes;
    public int num_vertexarrays, num_vertexes, ofs_vertexarrays;
    public int num_triangles, ofs_triangles, ofs_adjacency;
    public int num_joints, ofs_joints;
    public int num_poses, ofs_poses;
    public int num_anims, ofs_anims;
    public int num_frames, num_framechannels, ofs_frames, ofs_bounds;
    public int num_comment, ofs_comment;
    public int num_extensions, ofs_extensions;

    //objects
    /**
     * Dictionnary of Index->Chars
     */
    public Dictionary texts = new HashDictionary();
    public IQMMesh[] meshes;
    public IQMVertexArray[] vertexArays;
    public int[] triangles;
    public int[][] adjacencies;
    public IQMJoint[] joints;
    public IQMPose[] poses;
    public IQMAnim[] anims;
    public IQMBounds[] bounds;
    public Chars[] comments;
    public IQMExtension[] extensions;

    public void read(BacktrackInputStream bs) throws IOException {

        bs.mark();
        final DataInputStream ds = new DataInputStream(bs, Endianness.LITTLE_ENDIAN);

        if (!Arrays.equals(IQMStore.SIGNATURE, ds.readBytes(16))) {
            throw new FormatEncodingException(ds, "Wrong stream signature");
        }

        version             = ds.readInt();
        fileSize            = ds.readInt();
        flags               = ds.readInt();
        num_text            = ds.readInt();
        ofs_text            = ds.readInt();
        num_meshes          = ds.readInt();
        ofs_meshes          = ds.readInt();
        num_vertexarrays    = ds.readInt();
        num_vertexes        = ds.readInt();
        ofs_vertexarrays    = ds.readInt();
        num_triangles       = ds.readInt();
        ofs_triangles       = ds.readInt();
        ofs_adjacency       = ds.readInt();
        num_joints          = ds.readInt();
        ofs_joints          = ds.readInt();
        num_poses           = ds.readInt();
        ofs_poses           = ds.readInt();
        num_anims           = ds.readInt();
        ofs_anims           = ds.readInt();
        num_frames          = ds.readInt();
        num_framechannels   = ds.readInt();
        ofs_frames          = ds.readInt();
        ofs_bounds          = ds.readInt();
        num_comment         = ds.readInt();
        ofs_comment         = ds.readInt();
        num_extensions      = ds.readInt();
        ofs_extensions      = ds.readInt();

        bs.rewind();
        ds.skipFully(ofs_text);
        while (bs.position() < ofs_text+num_text) {
            int index = bs.position();
            texts.add(index-ofs_text,ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII));
        }

        bs.rewind();
        ds.skipFully(ofs_meshes);
        meshes = new IQMMesh[num_meshes];
        for (int i=0; i<num_meshes; i++) {
            meshes[i] = new IQMMesh();
            meshes[i].read(ds);
        }

        bs.rewind();
        ds.skipFully(ofs_vertexarrays);
        vertexArays = new IQMVertexArray[num_vertexarrays];
        for (int i=0; i<num_vertexarrays; i++) {
            vertexArays[i] = new IQMVertexArray();
            vertexArays[i].read(ds);
        }

        //rebuild vertex array datas
        for (int i=0; i<num_vertexarrays; i++) {
            bs.rewind();
            ds.skipFully(vertexArays[i].offset);
            //rebuild data array
            switch (vertexArays[i].format) {
                case IQMVertexArray.FORMAT_BYTE   : vertexArays[i].data = ds.readBytes(num_vertexes*vertexArays[i].size); break;
                case IQMVertexArray.FORMAT_UBYTE  : vertexArays[i].data = ds.readUByte(num_vertexes*vertexArays[i].size); break;
                case IQMVertexArray.FORMAT_SHORT  : vertexArays[i].data = ds.readShort(num_vertexes*vertexArays[i].size); break;
                case IQMVertexArray.FORMAT_USHORT : vertexArays[i].data = ds.readUShort(num_vertexes*vertexArays[i].size); break;
                case IQMVertexArray.FORMAT_INT    : vertexArays[i].data = ds.readInt(num_vertexes*vertexArays[i].size); break;
                case IQMVertexArray.FORMAT_UINT   : vertexArays[i].data = ds.readUInt(num_vertexes*vertexArays[i].size); break;
                case IQMVertexArray.FORMAT_HALF   : throw new FormatEncodingException(ds, "Unsupported vertex array format " + vertexArays[i].format);
                case IQMVertexArray.FORMAT_FLOAT  : vertexArays[i].data = ds.readFloat(num_vertexes*vertexArays[i].size); break;
                case IQMVertexArray.FORMAT_DOUBLE : vertexArays[i].data = ds.readDouble(num_vertexes*vertexArays[i].size); break;
                default : throw new FormatEncodingException(ds, "Unexpected vertex array format " + vertexArays[i].format);
            }
        }

        bs.rewind();
        ds.skipFully(ofs_triangles);
        triangles = ds.readInt(num_triangles*3);
//        triangles = new IQMTriangle[num_triangles];
//        for (int i=0; i<num_triangles; i++) {
//            triangles[i] = new IQMTriangle();
//            triangles[i].read(ds);
//        }

        bs.rewind();
        ds.skipFully(ofs_adjacency);
        adjacencies = new int[num_triangles][3];
        for (int i=0; i<num_triangles; i++) {
            adjacencies[i] = ds.readInt(3);
        }

        bs.rewind();
        ds.skipFully(ofs_joints);
        joints = new IQMJoint[num_joints];
        for (int i=0; i<num_joints; i++) {
            joints[i] = new IQMJoint();
            joints[i].read(ds);
        }

        bs.rewind();
        ds.skipFully(ofs_joints);
        poses = new IQMPose[num_poses];
        for (int i=0; i<num_poses; i++) {
            poses[i] = new IQMPose();
            poses[i].read(ds);
        }

        bs.rewind();
        ds.skipFully(ofs_anims);
        anims = new IQMAnim[num_anims];
        for (int i=0; i<num_anims; i++) {
            anims[i] = new IQMAnim();
            anims[i].read(ds);
        }

        //TODO Frames

        bs.rewind();
        ds.skipFully(ofs_bounds);
        bounds = new IQMBounds[num_frames];
        for (int i=0; i<num_frames; i++) {
            bounds[i] = new IQMBounds();
            bounds[i].read(ds);
        }

        bs.rewind();
        ds.skipFully(ofs_comment);
        comments = new Chars[num_comment];
        for (int i=0; i<num_comment; i++) {
            comments[i] = ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII);
        }

        bs.rewind();
        ds.skipFully(ofs_extensions);
        extensions = new IQMExtension[num_extensions];
        for (int i=0; i<num_extensions; i++) {
            extensions[i] = new IQMExtension();
            extensions[i].read(ds);
        }
    }

}
