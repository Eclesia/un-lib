
package science.unlicense.format.iqm;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 * Inter-Quake Export format.
 *
 * Resources :
 * http://sauerbraten.org/iqm/
 * http://sauerbraten.org/iqm/iqe.txt
 *
 * @author Johann Sorel
 */
public class IQEFormat extends AbstractModel3DFormat{

    public static final IQEFormat INSTANCE = new IQEFormat();

    private IQEFormat() {
        super(new Chars("IQE"));
        shortName = new Chars("IQE");
        longName = new Chars("Inter-Quake Export Format");
        extensions.add(new Chars("iqe"));
        signatures.add(IQEStore.SIGNATURE);
    }

    @Override
    public Store open(Object input) throws IOException {
        return new IQEStore(input);
    }

}
