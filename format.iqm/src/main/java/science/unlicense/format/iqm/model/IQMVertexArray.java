
package science.unlicense.format.iqm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class IQMVertexArray {

    public static final int TYPE_POSITION     = 0;  // float, 3
    public static final int TYPE_TEXCOORD     = 1;  // float, 2
    public static final int TYPE_NORMAL       = 2;  // float, 3
    public static final int TYPE_TANGENT      = 3;  // float, 4
    public static final int TYPE_BLENDINDEXES = 4;  // ubyte, 4
    public static final int TYPE_BLENDWEIGHTS = 5;  // ubyte, 4
    public static final int TYPE_COLOR        = 6;  // ubyte, 4
    public static final int TYPE_CUSTOM       = 0x10;

    public static final int FORMAT_BYTE   = 0;
    public static final int FORMAT_UBYTE  = 1;
    public static final int FORMAT_SHORT  = 2;
    public static final int FORMAT_USHORT = 3;
    public static final int FORMAT_INT    = 4;
    public static final int FORMAT_UINT   = 5;
    public static final int FORMAT_HALF   = 6;
    public static final int FORMAT_FLOAT  = 7;
    public static final int FORMAT_DOUBLE = 8;

    /** type or custom name */
    public int type;
    public int flags;
    /**
     * Component format.
     */
    public int format;
    /**
     * number of components
     */
    public int size;
    /**
     * offset to array of tightly packed components, with num_vertexes * size total entries
     * offset must be aligned to max(sizeof(format), 4)
     */
    public int offset;

    /**
     * Loaded vertex array data.
     * Object is a primitive array of vertex array type.
     */
    public Object data;

    public void read(DataInputStream ds) throws IOException {
        type = ds.readInt();
        flags = ds.readInt();
        format = ds.readInt();
        size = ds.readInt();
        offset = ds.readInt();
    }

}
