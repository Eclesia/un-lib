
package science.unlicense.format.iqm;

import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float32;
import science.unlicense.common.api.number.Int32;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.CharInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class IQEReader extends AbstractReader {

    public static final Chars COMMENT = Chars.constant("#",CharEncodings.UTF_8);
    public static final Chars VERTEX_POS = Chars.constant("vp",CharEncodings.UTF_8);
    public static final Chars VERTEX_TEX = Chars.constant("vt",CharEncodings.UTF_8);
    public static final Chars VERTEX_NOR = Chars.constant("vn",CharEncodings.UTF_8);
    public static final Chars VERTEX_BLE = Chars.constant("vb",CharEncodings.UTF_8);
    public static final Chars VERTEX_COL = Chars.constant("vc",CharEncodings.UTF_8);
    public static final Chars VERTEX_CUS = Chars.constant("v",CharEncodings.UTF_8);
    public static final Chars VERTEX_ARRAY = Chars.constant("vertexarray",CharEncodings.UTF_8);
    public static final Chars MESH = Chars.constant("mesh",CharEncodings.UTF_8);
    public static final Chars MATERIAL = Chars.constant("material",CharEncodings.UTF_8);
    public static final Chars TRIANGLE_FROM_START = Chars.constant("fa",CharEncodings.UTF_8);
    public static final Chars TRIANGLE_FROM_MESH = Chars.constant("fm",CharEncodings.UTF_8);
    public static final Chars SMOOTH_UV = Chars.constant("smoothuv",CharEncodings.UTF_8);
    public static final Chars SMOOTH_GROUP = Chars.constant("smoothgroup",CharEncodings.UTF_8);
    public static final Chars SMOOTH_ANGLE = Chars.constant("smoothangle",CharEncodings.UTF_8);
    public static final Chars SMOOTH_VERTEX = Chars.constant("vs",CharEncodings.UTF_8);
    public static final Chars SMOOTH_TRIANGLE = Chars.constant("fs",CharEncodings.UTF_8);
    public static final Chars POSE_QUATERNION = Chars.constant("pq",CharEncodings.UTF_8);
    public static final Chars POSE_MATRIX = Chars.constant("pm",CharEncodings.UTF_8);
    public static final Chars POSE_ANGLE = Chars.constant("pa",CharEncodings.UTF_8);
    public static final Chars JOINT = Chars.constant("joint",CharEncodings.UTF_8);
    public static final Chars ANIMATION = Chars.constant("animation",CharEncodings.UTF_8);
    public static final Chars LOOP = Chars.constant("loop",CharEncodings.UTF_8);
    public static final Chars FRAMERATE = Chars.constant("framerate",CharEncodings.UTF_8);
    public static final Chars FRAME = Chars.constant("frame",CharEncodings.UTF_8);

    /**
     * Get next object in stream.
     *
     * @return next record or null if no more records
     */
    public Record next() throws IOException {
        final CharInputStream cs = getInputAsCharStream(CharEncodings.UTF_8);

        for (Chars line = cs.readLine();line!=null;line=cs.readLine()) {
            line = line.trim();
            if (line.isEmpty()) continue;

            final Record rec = new Record();
            if (line.startsWith(COMMENT)) {
                rec.type = COMMENT;
                rec.values = new Object[]{line.truncate(-1, 0)};
                return rec;
            }

            final Chars[] parts = line.split(' ');
            rec.type = parts[0];
            rec.values = new Object[parts.length-1];
            for (int i=1;i<parts.length;i++) {
                try {
                    rec.values[i-1] = Int32.decode(parts[i]);
                } catch(RuntimeException ex) {
                    try {
                        rec.values[i-1] = Float32.decode(parts[i]);
                    } catch(RuntimeException ex2) {
                        rec.values[i-1] = parts[i];
                    }
                }
            }

            return rec;
        }

        return null;
    }

    public static class Record {
        public Chars type;
        public Object[] values;
    }

}
