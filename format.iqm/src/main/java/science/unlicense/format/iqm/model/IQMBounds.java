
package science.unlicense.format.iqm.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class IQMBounds {

    public float[] bbmins;
    public float[] bbmaxs;
    public float xyradius;
    public float radius;

    public void read(DataInputStream ds) throws IOException {
        bbmins = ds.readFloat(3);
        bbmaxs = ds.readFloat(3);
        xyradius = ds.readFloat();
        radius = ds.readFloat();
    }

}
