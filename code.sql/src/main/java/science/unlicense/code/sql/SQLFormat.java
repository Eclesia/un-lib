
package science.unlicense.code.sql;

import science.unlicense.code.api.CodeFileReader;
import science.unlicense.code.api.CodeFormat;
import science.unlicense.code.api.CodeProducer;
import science.unlicense.code.impl.GrammarCodeFileReader;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public class SQLFormat extends DefaultFormat implements CodeFormat {

    public static final SQLFormat INSTANCE = new SQLFormat();

    private SQLFormat() {
        super(new Chars("sql"));
        shortName = new Chars("SQL");
        longName = new Chars("Structured Query Language file");
        extensions.add(new Chars("sql"));
    }

    @Override
    public CodeFileReader createReader() {
        return new GrammarCodeFileReader(null);
    }

    @Override
    public CodeProducer createProducer() {
        return null;
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
