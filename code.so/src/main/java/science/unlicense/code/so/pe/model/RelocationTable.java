

package science.unlicense.code.so.pe.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.code.so.pe.SectionHeader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class RelocationTable extends Section {

    public static final class BaseRelocationBlock{
        /** 4bytes */
        public int PageRVA;
        /** 4bytes */
        public int BlockSize;
        /** types :
         * - 4 bits Type
         * - 12 bits Offset
         */
        public short[] types;
    }

    public static final class Relocation{
        /** 4bytes */
        public int VirtualAddress;
        /** 4bytes */
        public int SymbolTableIndex;
        /** 2bytes */
        public short type;
    }

    public Relocation[] relocations;

    public RelocationTable(SectionHeader header) {
        super(header, new Chars(".reloc"));
    }

    public void read(DataInputStream ds) throws IOException {
        super.read(ds);
        //TODO
    }

}
