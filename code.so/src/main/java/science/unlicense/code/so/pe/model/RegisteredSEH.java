

package science.unlicense.code.so.pe.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.code.so.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class RegisteredSEH extends Section {

    public RegisteredSEH(SectionHeader header) {
        super(header, new Chars(".idlsym"));
    }

}
