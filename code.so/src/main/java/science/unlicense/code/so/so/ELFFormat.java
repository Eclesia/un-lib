
package science.unlicense.code.so.so;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.encoding.api.store.Store;

/**
 *
 * Informations :
 * http://en.wikipedia.org/wiki/Executable_and_Linkable_Format
 * http://man7.org/linux/man-pages/man5/elf.5.html
 * http://wiki.osdev.org/ELF
 * https://refspecs.linuxbase.org/
 *
 * @author Johann Sorel
 */
public class ELFFormat extends DefaultFormat {

    public static final ELFFormat INSTANCE = new ELFFormat();

    private ELFFormat() {
        super(new Chars("ELF"));
        shortName = new Chars("ELF");
        longName = new Chars("Executable and Linkable Format");
        extensions.add(new Chars("axf"));
        extensions.add(new Chars("bin"));
        extensions.add(new Chars("elf"));
        extensions.add(new Chars("o"));
        extensions.add(new Chars("prx"));
        extensions.add(new Chars("puff"));
        extensions.add(new Chars("so"));
        signatures.add(ELFConstants.SIGNATURE);
    }

    @Override
    public Store open(Object source) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
