

package science.unlicense.code.so.pe.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.code.so.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class ExportTable extends Section {

    public static class ExportDirectoryTable{
        /** 4bytes */
        public int ExportFlags;
        /** 4bytes */
        public int DateStamp;
        /** 2bytes */
        public short MajorVersion;
        /** 2bytes */
        public short MinorVersion;
        /** 4bytes */
        public int NameRVA;
        /** 4bytes */
        public int OrdinalBase;
        /** 4bytes */
        public int AddressTableEntries;
        /** 4bytes */
        public int NumberofNamePointers;
        /** 4bytes */
        public int ExportAddressTableRVA;
        /** 4bytes */
        public int NamePointerRVA;
        /** 4bytes */
        public int OrdinalTableRVA;
    }

    public static class ExportAddressTable{
        /** 4bytes */
        public int Export_RVA;
        /** 4bytes */
        public int Forwarder_RVA;

    }

    public ExportTable(SectionHeader header) {
        super(header, new Chars(".edata"));
    }

}
