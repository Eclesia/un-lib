
package science.unlicense.code.so.so.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.code.so.so.ELFConstants;

/**
 *
 * @author Johann Sorel
 */
public class ELFRelocation {

    /** Elf64_Addr */
    public long r_offset;
    /** uint64_t */
    public long r_info;
    /** int64_t */
    public long r_addend;

    public void read(DataInputStream ds, ELFHeader header, boolean addend) throws IOException{

        if (header.encodingsize == ELFConstants.ENC_32) {
            r_offset = ds.readUInt();
            r_info = ds.readUInt();
            if (addend) {
                r_addend = ds.readUInt();
            }
        } else {
            r_offset = ds.readLong();
            r_info = ds.readLong();
            if (addend) {
                r_addend = ds.readLong();
            }
        }
    }

}
