

package science.unlicense.code.so.pe.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.code.so.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class ThreadLocalStorageTable extends Section {

    /** 4/8bytes*/
    public long RawDataStartVA;
    /** 4/8bytes*/
    public long RawDataEndVA;
    /** 4/8bytes*/
    public long AddressOfIndex;
    /** 4/8bytes*/
    public long AddressOfCallbacks;
    /** 4bytes*/
    public int SizeOfZeroFill;
    /** 4bytes*/
    public int Characteristics;


    public ThreadLocalStorageTable(SectionHeader header) {
        super(header, new Chars(".tls"));
    }

}
