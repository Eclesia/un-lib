
package science.unlicense.code.so.so;

import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.code.so.so.model.ELFFile;
import science.unlicense.encoding.impl.io.BacktrackInputStream;

/**
 * Executable Link File reader.
 *
 * @author Johann Sorel
 */
public class ELFReader extends AbstractReader{

    public ELFReader() {
    }

    public ELFFile read() throws IOException{
        final BacktrackInputStream ds = getInputAsBacktrackStream();
        final ELFFile file = new ELFFile();
        file.read(ds);
        return file;
    }

}
