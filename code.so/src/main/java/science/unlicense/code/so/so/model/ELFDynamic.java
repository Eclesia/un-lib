
package science.unlicense.code.so.so.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.code.so.so.ELFConstants;

/**
 *
 * @author Johann Sorel
 */
public class ELFDynamic {

    /** Elf32_Sword */
    public long d_tag;
    /** Elf32_Word    or d_ptr */
    public long d_val;

    public void read(DataInputStream ds, ELFHeader header) throws IOException{

        if (header.encodingsize == ELFConstants.ENC_32) {
            d_tag = ds.readInt();
            d_val = ds.readUInt();
        } else {
            d_tag = ds.readLong();
            d_val = ds.readLong();
        }
    }

}
