
package science.unlicense.code.so.pe;

import science.unlicense.code.so.pe.model.Section;

/**
 *
 * @author Johann Sorel
 */
public class PortableExecutable {

    public DosHeader dosHeader;
    public DosCode dosCode;

    public PEHeader peHeader;

    public SectionHeader[] sectionHeaders;
    public Section[] sections;

}
