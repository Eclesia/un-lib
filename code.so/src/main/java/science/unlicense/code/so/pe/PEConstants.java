
package science.unlicense.code.so.pe;

/**
 *
 * @author Johann Sorel
 */
public final class PEConstants {

    public static final short DOS_HEADER = 0x5a4d; // MZ

    public static final int PE_HEADER = 0x00004550; // PE/0/0

    /** The file is an executable image. */
    public static final int IMAGE_NT_OPTIONAL_HDR32_MAGIC = 0x10b;
    /** The file is an executable image. */
    public static final int IMAGE_NT_OPTIONAL_HDR64_MAGIC = 0x20b;
    /** The file is a ROM image */
    public static final int IMAGE_ROM_OPTIONAL_HDR_MAGIC = 0x107;

    // File header Machine possible values
    public static final int IMAGE_FILE_MACHINE_UNKNOWN = 0x00;
    public static final int IMAGE_FILE_MACHINE_AM33 = 0x1d3;
    public static final int IMAGE_FILE_MACHINE_AMD64 = 0x8664;
    public static final int IMAGE_FILE_MACHINE_ARM = 0x1c0;
    public static final int IMAGE_FILE_MACHINE_ARMNT = 0x1c4;
    public static final int IMAGE_FILE_MACHINE_ARM64 = 0xaa64;
    public static final int IMAGE_FILE_MACHINE_EBC = 0xebc;
    public static final int IMAGE_FILE_MACHINE_I386 = 0x14c;
    public static final int IMAGE_FILE_MACHINE_IA64 = 0x200;
    public static final int IMAGE_FILE_MACHINE_M32R = 0x9041;
    public static final int IMAGE_FILE_MACHINE_MIPS16 = 0x266;
    public static final int IMAGE_FILE_MACHINE_MIPSFPU = 0x366;
    public static final int IMAGE_FILE_MACHINE_MIPSFPU16 = 0x466;
    public static final int IMAGE_FILE_MACHINE_POWERPC = 0x1f0;
    public static final int IMAGE_FILE_MACHINE_POWERPCFP = 0x1f1;
    public static final int IMAGE_FILE_MACHINE_R4000 = 0x166;
    public static final int IMAGE_FILE_MACHINE_SH3 = 0x1a2;
    public static final int IMAGE_FILE_MACHINE_SH3DSP = 0x1a3;
    public static final int IMAGE_FILE_MACHINE_SH4 = 0x1a6;
    public static final int IMAGE_FILE_MACHINE_SH5 = 0x1a8;
    public static final int IMAGE_FILE_MACHINE_THUMB = 0x1c2;
    public static final int IMAGE_FILE_MACHINE_WCEMIPSV2 = 0x169;

    // File header Characteristics possible values
    public static final int IMAGE_FILE_RELOCS_STRIPPED = 0x0001;
    public static final int IMAGE_FILE_EXECUTABLE_IMAGE = 0x0002;
    public static final int IMAGE_FILE_LINE_NUMS_STRIPPED = 0x0004;
    public static final int IMAGE_FILE_LOCAL_SYMS_STRIPPED = 0x0008;
    public static final int IMAGE_FILE_AGGRESSIVE_WS_TRIM = 0x0010;
    public static final int IMAGE_FILE_LARGE_ADDRESS_AWARE = 0x0020;
    public static final int IMAGE_FILE_BYTES_REVERSED_LO = 0x0080;
    public static final int IMAGE_FILE_32BIT_MACHINE = 0x0100;
    public static final int IMAGE_FILE_DEBUG_STRIPPED = 0x0200;
    public static final int IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP = 0x0400;
    public static final int IMAGE_FILE_NET_RUN_FROM_SWAP = 0x0800;
    public static final int IMAGE_FILE_SYSTEM = 0x1000;
    public static final int IMAGE_FILE_DLL = 0x2000;
    public static final int IMAGE_FILE_UP_SYSTEM_ONLY = 0x4000;
    public static final int IMAGE_FILE_BYTES_REVERSED_HI = 0x8000;

    //Optional header Subsystem possible values
    public static final int IMAGE_SUBSYSTEM_UNKNOWN = 0;
    public static final int IMAGE_SUBSYSTEM_NATIVE = 1;
    public static final int IMAGE_SUBSYSTEM_WINDOWS_GUI = 2;
    public static final int IMAGE_SUBSYSTEM_WINDOWS_CUI = 3;
    public static final int IMAGE_SUBSYSTEM_POSIX_CUI = 7;
    public static final int IMAGE_SUBSYSTEM_WINDOWS_CE_GUI = 9;
    public static final int IMAGE_SUBSYSTEM_EFI_APPLICATION = 10;
    public static final int IMAGE_SUBSYSTEM_EFI_BOOT_SERVICE_DRIVER = 11;
    public static final int IMAGE_SUBSYSTEM_EFI_RUNTIME_DRIVER = 12;
    public static final int IMAGE_SUBSYSTEM_EFI_ROM = 13;
    public static final int IMAGE_SUBSYSTEM_XBOX = 14;

    //Optional header DLLCharacteristic possible values
    public static final int IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE = 0x0040;
    public static final int IMAGE_DLLCHARACTERISTICS_FORCE_INTEGRITY = 0x0080;
    public static final int IMAGE_DLLCHARACTERISTICS_NX_COMPAT = 0x0100;
    public static final int IMAGE_DLLCHARACTERISTICS_NO_ISOLATION = 0x0200;
    public static final int IMAGE_DLLCHARACTERISTICS_NO_SEH = 0x0400;
    public static final int IMAGE_DLLCHARACTERISTICS_NO_BIND = 0x0800;
    public static final int IMAGE_DLLCHARACTERISTICS_WDM_DRIVER = 0x2000;
    public static final int IMAGE_DLLCHARACTERISTICS_TERMINAL_SERVER_AWARE = 0x8000;

    //Section header Characterics possible values
    public static final int IMAGE_SCN_TYPE_NO_PAD = 0x00000008;
    public static final int IMAGE_SCN_CNT_CODE = 0x00000020;
    public static final int IMAGE_SCN_CNT_INITIALIZED_DATA = 0x00000040;
    public static final int IMAGE_SCN_CNT_UNINITIALIZED_DATA = 0x00000080;
    public static final int IMAGE_SCN_LNK_OTHER = 0x00000100;
    public static final int IMAGE_SCN_LNK_INFO = 0x00000200;
    public static final int IMAGE_SCN_LNK_REMOVE = 0x00000800;
    public static final int IMAGE_SCN_LNK_COMDAT = 0x00001000;
    public static final int IMAGE_SCN_GPREL = 0x00008000;
    public static final int IMAGE_SCN_MEM_PURGEABLE = 0x00020000;
    public static final int IMAGE_SCN_MEM_16BIT = 0x00020000;
    public static final int IMAGE_SCN_MEM_LOCKED = 0x00040000;
    public static final int IMAGE_SCN_MEM_PRELOAD = 0x00080000;
    public static final int IMAGE_SCN_ALIGN_1BYTES = 0x00100000;
    public static final int IMAGE_SCN_ALIGN_2BYTES = 0x00200000;
    public static final int IMAGE_SCN_ALIGN_4BYTES = 0x00300000;
    public static final int IMAGE_SCN_ALIGN_8BYTES = 0x00400000;
    public static final int IMAGE_SCN_ALIGN_16BYTES = 0x00500000;
    public static final int IMAGE_SCN_ALIGN_32BYTES = 0x00600000;
    public static final int IMAGE_SCN_ALIGN_64BYTES = 0x00700000;
    public static final int IMAGE_SCN_ALIGN_128BYTES = 0x00800000;
    public static final int IMAGE_SCN_ALIGN_256BYTES = 0x00900000;
    public static final int IMAGE_SCN_ALIGN_512BYTES = 0x00A00000;
    public static final int IMAGE_SCN_ALIGN_1024BYTES = 0x00B00000;
    public static final int IMAGE_SCN_ALIGN_2048BYTES = 0x00C00000;
    public static final int IMAGE_SCN_ALIGN_4096BYTES = 0x00D00000;
    public static final int IMAGE_SCN_ALIGN_8192BYTES = 0x00E00000;
    public static final int IMAGE_SCN_LNK_NRELOC_OVFL = 0x01000000;
    public static final int IMAGE_SCN_MEM_DISCARDABLE = 0x02000000;
    public static final int IMAGE_SCN_MEM_NOT_CACHED = 0x04000000;
    public static final int IMAGE_SCN_MEM_NOT_PAGED = 0x08000000;
    public static final int IMAGE_SCN_MEM_SHARED = 0x10000000;
    public static final int IMAGE_SCN_MEM_EXECUTE = 0x20000000;
    public static final int IMAGE_SCN_MEM_READ = 0x40000000;
    public static final int IMAGE_SCN_MEM_WRITE = 0x80000000;

    // Relocation possible values
    public static final int IMAGE_REL_BASED_ABSOLUTE = 0;
    public static final int IMAGE_REL_BASED_HIGH = 1;
    public static final int IMAGE_REL_BASED_LOW = 2;
    public static final int IMAGE_REL_BASED_HIGHLOW = 3;
    public static final int IMAGE_REL_BASED_HIGHADJ = 4;
    public static final int IMAGE_REL_BASED_MIPS_JMPADDR = 5;
    public static final int IMAGE_REL_BASED_ARM_MOV32A =  5;
    public static final int IMAGE_REL_BASED_ARM_MOV32T = 7;
    public static final int IMAGE_REL_BASED_MIPS_JMPADDR16 = 9;
    public static final int IMAGE_REL_BASED_DIR64 = 10;

    //X86-64
    public static final int IMAGE_REL_AMD64_ABSOLUTE = 0x0000;
    public static final int IMAGE_REL_AMD64_ADDR64 = 0x0001;
    public static final int IMAGE_REL_AMD64_ADDR32 = 0x0002;
    public static final int IMAGE_REL_AMD64_ADDR32NB = 0x0003;
    public static final int IMAGE_REL_AMD64_REL32 = 0x0004;
    public static final int IMAGE_REL_AMD64_REL32_1 = 0x0005;
    public static final int IMAGE_REL_AMD64_REL32_2 = 0x0006;
    public static final int IMAGE_REL_AMD64_REL32_3 = 0x0007;
    public static final int IMAGE_REL_AMD64_REL32_4 = 0x0008;
    public static final int IMAGE_REL_AMD64_REL32_5 = 0x0009;
    public static final int IMAGE_REL_AMD64_SECTION = 0x000A;
    public static final int IMAGE_REL_AMD64_SECREL = 0x000B;
    public static final int IMAGE_REL_AMD64_SECREL7 = 0x000C;
    public static final int IMAGE_REL_AMD64_TOKEN = 0x000D;
    public static final int IMAGE_REL_AMD64_SREL32 = 0x000E;
    public static final int IMAGE_REL_AMD64_PAIR = 0x000F;
    public static final int IMAGE_REL_AMD64_SSPAN32 = 0x0010;

    //ARM
    public static final int IMAGE_REL_ARM_ABSOLUTE = 0x0000;
    public static final int IMAGE_REL_ARM_ADDR32 = 0x0001;
    public static final int IMAGE_REL_ARM_ADDR32NB = 0x0002;
    public static final int IMAGE_REL_ARM_BRANCH24 = 0x0003;
    public static final int IMAGE_REL_ARM_BRANCH11 = 0x0004;
    public static final int IMAGE_REL_ARM_TOKEN = 0x0005;
    public static final int IMAGE_REL_ARM_BLX24 = 0x0008;
    public static final int IMAGE_REL_ARM_BLX11 = 0x0009;
    public static final int IMAGE_REL_ARM_SECTION = 0x000E;
    public static final int IMAGE_REL_ARM_SECREL = 0x000F;
    public static final int IMAGE_REL_ARM_MOV32A = 0x0010;
    public static final int IMAGE_REL_ARM_MOV32T = 0x0011;
    public static final int IMAGE_REL_ARM_BRANCH20T = 0x0012;
    public static final int IMAGE_REL_ARM_BRANCH24T = 0x0014;
    public static final int IMAGE_REL_ARM_BLX23T = 0x0015;

    //ARMv8-64bits
    public static final int IMAGE_REL_ARM64_ABSOLUTE = 0x0000;
    public static final int IMAGE_REL_ARM64_ADDR32 = 0x0001;
    public static final int IMAGE_REL_ARM64_ADDR32NB = 0x0002;
    public static final int IMAGE_REL_ARM64_BRANCH26 = 0x0003;
    public static final int IMAGE_REL_ARM64_PAGEBASE_REL21 = 0x0004;
    public static final int IMAGE_REL_ARM64_REL21 = 0x0005;
    public static final int IMAGE_REL_ARM64_PAGEOFFSET_12A = 0x0006;
    public static final int IMAGE_REL_ARM64_PAGEOFFSET_12L = 0x0007;
    public static final int IMAGE_REL_ARM64_SECREL = 0x0008;
    public static final int IMAGE_REL_ARM64_SECREL_LOW12A = 0x0009;
    public static final int IMAGE_REL_ARM64_SECREL_HIGH12A = 0x000A;
    public static final int IMAGE_REL_ARM64_SECREL_LOW12L = 0x000B;
    public static final int IMAGE_REL_ARM64_TOKEN = 0x000C;
    public static final int IMAGE_REL_ARM64_SECTION = 0x000D;
    public static final int IMAGE_REL_ARM64_ADDR64 = 0x000E;

    //Hitachi SuperH
    public static final int IMAGE_REL_SH3_ABSOLUTE = 0x0000;
    public static final int IMAGE_REL_SH3_DIRECT16 = 0x0001;
    public static final int IMAGE_REL_SH3_DIRECT32 = 0x0002;
    public static final int IMAGE_REL_SH3_DIRECT8 = 0x0003;
    public static final int IMAGE_REL_SH3_DIRECT8_WORD = 0x0004;
    public static final int IMAGE_REL_SH3_DIRECT8_LONG = 0x0005;
    public static final int IMAGE_REL_SH3_DIRECT4 = 0x0006;
    public static final int IMAGE_REL_SH3_DIRECT4_WORD = 0x0007;
    public static final int IMAGE_REL_SH3_DIRECT4_LONG = 0x0008;
    public static final int IMAGE_REL_SH3_PCREL8_WORD = 0x0009;
    public static final int IMAGE_REL_SH3_PCREL8_LONG = 0x000A;
    public static final int IMAGE_REL_SH3_PCREL12_WORD = 0x000B;
    public static final int IMAGE_REL_SH3_STARTOF_SECTION = 0x000C;
    public static final int IMAGE_REL_SH3_SIZEOF_SECTION = 0x000D;
    public static final int IMAGE_REL_SH3_SECTION = 0x000E;
    public static final int IMAGE_REL_SH3_SECREL = 0x000F;
    public static final int IMAGE_REL_SH3_DIRECT32_NB = 0x0010;
    public static final int IMAGE_REL_SH3_GPREL4_LONG = 0x0011;
    public static final int IMAGE_REL_SH3_TOKEN = 0x0012;
    public static final int IMAGE_REL_SHM_PCRELPT = 0x0013;
    public static final int IMAGE_REL_SHM_REFLO = 0x0014;
    public static final int IMAGE_REL_SHM_REFHALF = 0x0015;
    public static final int IMAGE_REL_SHM_RELLO = 0x0016;
    public static final int IMAGE_REL_SHM_RELHALF = 0x0017;
    public static final int IMAGE_REL_SHM_PAIR = 0x0018;
    public static final int IMAGE_REL_SHM_NOMODE = 0x8000;

    //IBM PowerPC
    public static final int IMAGE_REL_PPC_ABSOLUTE = 0x0000;
    public static final int IMAGE_REL_PPC_ADDR64 = 0x0001;
    public static final int IMAGE_REL_PPC_ADDR32 = 0x0002;
    public static final int IMAGE_REL_PPC_ADDR24 = 0x0003;
    public static final int IMAGE_REL_PPC_ADDR16 = 0x0004;
    public static final int IMAGE_REL_PPC_ADDR14 = 0x0005;
    public static final int IMAGE_REL_PPC_REL24 = 0x0006;
    public static final int IMAGE_REL_PPC_REL14 = 0x0007;
    public static final int IMAGE_REL_PPC_ADDR32NB = 0x000A;
    public static final int IMAGE_REL_PPC_SECREL = 0x000B;
    public static final int IMAGE_REL_PPC_SECTION = 0x000C;
    public static final int IMAGE_REL_PPC_SECREL16 = 0x000F;
    public static final int IMAGE_REL_PPC_REFHI = 0x0010;
    public static final int IMAGE_REL_PPC_REFLO = 0x0011;
    public static final int IMAGE_REL_PPC_PAIR = 0x0012;
    public static final int IMAGE_REL_PPC_SECRELLO = 0x0013;
    public static final int IMAGE_REL_PPC_GPREL = 0x0015;
    public static final int IMAGE_REL_PPC_TOKEN = 0x0016;

    //Intel 386
    public static final int IMAGE_REL_I386_ABSOLUTE = 0x0000;
    public static final int IMAGE_REL_I386_DIR16 = 0x0001;
    public static final int IMAGE_REL_I386_REL16 = 0x0002;
    public static final int IMAGE_REL_I386_DIR32 = 0x0006;
    public static final int IMAGE_REL_I386_DIR32NB = 0x0007;
    public static final int IMAGE_REL_I386_SEG12 = 0x0009;
    public static final int IMAGE_REL_I386_SECTION = 0x000A;
    public static final int IMAGE_REL_I386_SECREL = 0x000B;
    public static final int IMAGE_REL_I386_TOKEN = 0x000C;
    public static final int IMAGE_REL_I386_SECREL7 = 0x000D;
    public static final int IMAGE_REL_I386_REL32 = 0x0014;

    //Intel Itanium
    public static final int IMAGE_REL_IA64_ABSOLUTE = 0x0000;
    public static final int IMAGE_REL_IA64_IMM14 = 0x0001;
    public static final int IMAGE_REL_IA64_IMM22 = 0x0002;
    public static final int IMAGE_REL_IA64_IMM64 = 0x0003;
    public static final int IMAGE_REL_IA64_DIR32 = 0x0004;
    public static final int IMAGE_REL_IA64_DIR64 = 0x0005;
    public static final int IMAGE_REL_IA64_PCREL21B = 0x0006;
    public static final int IMAGE_REL_IA64_PCREL21M = 0x0007;
    public static final int IMAGE_REL_IA64_PCREL21F = 0x0008;
    public static final int IMAGE_REL_IA64_GPREL22 = 0x0009;
    public static final int IMAGE_REL_IA64_LTOFF22 = 0x000A;
    public static final int IMAGE_REL_IA64_SECTION = 0x000B;
    public static final int IMAGE_REL_IA64_SECREL22 = 0x000C;
    public static final int IMAGE_REL_IA64_SECREL64I = 0x000D;
    public static final int IMAGE_REL_IA64_SECREL32 = 0x000E;
    public static final int IMAGE_REL_IA64_DIR32NB = 0x0010;
    public static final int IMAGE_REL_IA64_SREL14 = 0x0011;
    public static final int IMAGE_REL_IA64_SREL22 = 0x0012;
    public static final int IMAGE_REL_IA64_SREL32 = 0x0013;
    public static final int IMAGE_REL_IA64_UREL32 = 0x0014;
    public static final int IMAGE_REL_IA64_PCREL60X = 0x0015;
    public static final int IMAGE_REL_IA64_PCREL60B = 0x0016;
    public static final int IMAGE_REL_IA64_PCREL60F = 0x0017;
    public static final int IMAGE_REL_IA64_PCREL60I = 0x0018;
    public static final int IMAGE_REL_IA64_PCREL60M = 0x0019;
    public static final int IMAGE_REL_IA64_IMMGPREL64 = 0x001a;
    public static final int IMAGE_REL_IA64_TOKEN = 0x001b;
    public static final int IMAGE_REL_IA64_GPREL32 = 0x001c;
    public static final int IMAGE_REL_IA64_ADDEND = 0x001F;

    //MIPS
    public static final int IMAGE_REL_MIPS_ABSOLUTE = 0x0000;
    public static final int IMAGE_REL_MIPS_REFHALF = 0x0001;
    public static final int IMAGE_REL_MIPS_REFWORD = 0x0002;
    public static final int IMAGE_REL_MIPS_JMPADDR = 0x0003;
    public static final int IMAGE_REL_MIPS_REFHI = 0x0004;
    public static final int IMAGE_REL_MIPS_REFLO = 0x0005;
    public static final int IMAGE_REL_MIPS_GPREL = 0x0006;
    public static final int IMAGE_REL_MIPS_LITERAL = 0x0007;
    public static final int IMAGE_REL_MIPS_SECTION = 0x000A;
    public static final int IMAGE_REL_MIPS_SECREL = 0x000B;
    public static final int IMAGE_REL_MIPS_SECRELLO = 0x000C;
    public static final int IMAGE_REL_MIPS_SECRELHI = 0x000D;
    public static final int IMAGE_REL_MIPS_JMPADDR16 = 0x0010;
    public static final int IMAGE_REL_MIPS_REFWORDNB = 0x0022;
    public static final int IMAGE_REL_MIPS_PAIR = 0x0025;

    //mitsubishi M32R
    public static final int IMAGE_REL_M32R_ABSOLUTE = 0x0000;
    public static final int IMAGE_REL_M32R_ADDR32 = 0x0001;
    public static final int IMAGE_REL_M32R_ADDR32NB = 0x0002;
    public static final int IMAGE_REL_M32R_ADDR24 = 0x0003;
    public static final int IMAGE_REL_M32R_GPREL16 = 0x0004;
    public static final int IMAGE_REL_M32R_PCREL24 = 0x0005;
    public static final int IMAGE_REL_M32R_PCREL16 = 0x0006;
    public static final int IMAGE_REL_M32R_PCREL8 = 0x0007;
    public static final int IMAGE_REL_M32R_REFHALF = 0x0008;
    public static final int IMAGE_REL_M32R_REFHI = 0x0009;
    public static final int IMAGE_REL_M32R_REFLO = 0x000A;
    public static final int IMAGE_REL_M32R_PAIR = 0x000B;
    public static final int IMAGE_REL_M32R_SECTION = 0x000C;
    public static final int IMAGE_REL_M32R_SECREL = 0x000D;
    public static final int IMAGE_REL_M32R_TOKEN = 0x000E;

    //used in symbol table
    public static final int IMAGE_SYM_UNDEFINED = 0;
    public static final int IMAGE_SYM_ABSOLUTE = -1;
    public static final int IMAGE_SYM_DEBUG = -2;

    // Symbol table value types
    public static final int IMAGE_SYM_TYPE_NULL = 0;
    public static final int IMAGE_SYM_TYPE_VOID = 1;
    public static final int IMAGE_SYM_TYPE_CHAR = 2;
    public static final int IMAGE_SYM_TYPE_SHORT = 3;
    public static final int IMAGE_SYM_TYPE_INT = 4;
    public static final int IMAGE_SYM_TYPE_LONG = 5;
    public static final int IMAGE_SYM_TYPE_FLOAT = 6;
    public static final int IMAGE_SYM_TYPE_DOUBLE = 7;
    public static final int IMAGE_SYM_TYPE_STRUCT = 8;
    public static final int IMAGE_SYM_TYPE_UNION = 9;
    public static final int IMAGE_SYM_TYPE_ENUM = 10;
    public static final int IMAGE_SYM_TYPE_MOE = 11;
    public static final int IMAGE_SYM_TYPE_BYTE = 12;
    public static final int IMAGE_SYM_TYPE_WORD = 13;
    public static final int IMAGE_SYM_TYPE_UINT = 14;
    public static final int IMAGE_SYM_TYPE_DWORD = 15;

    public static final int IMAGE_SYM_DTYPE_NULL = 0;
    public static final int IMAGE_SYM_DTYPE_POINTER = 1;
    public static final int IMAGE_SYM_DTYPE_FUNCTION = 2;
    public static final int IMAGE_SYM_DTYPE_ARRAY = 3;

    // Symbol table classes
    public static final int IMAGE_SYM_CLASS_END_OF_FUNCTION = -1; // (0xFF)
    public static final int IMAGE_SYM_CLASS_NULL = 0;
    public static final int IMAGE_SYM_CLASS_AUTOMATIC = 1;
    public static final int IMAGE_SYM_CLASS_EXTERNAL = 2;
    public static final int IMAGE_SYM_CLASS_STATIC = 3;
    public static final int IMAGE_SYM_CLASS_REGISTER = 4;
    public static final int IMAGE_SYM_CLASS_EXTERNAL_DEF = 5;
    public static final int IMAGE_SYM_CLASS_LABEL = 6;
    public static final int IMAGE_SYM_CLASS_UNDEFINED_LABEL = 7;
    public static final int IMAGE_SYM_CLASS_MEMBER_OF_STRUCT = 8;
    public static final int IMAGE_SYM_CLASS_ARGUMENT = 9;
    public static final int IMAGE_SYM_CLASS_STRUCT_TAG = 10;
    public static final int IMAGE_SYM_CLASS_MEMBER_OF_UNION = 11;
    public static final int IMAGE_SYM_CLASS_UNION_TAG = 12;
    public static final int IMAGE_SYM_CLASS_TYPE_DEFINITION = 13;
    public static final int IMAGE_SYM_CLASS_UNDEFINED_STATIC = 14;
    public static final int IMAGE_SYM_CLASS_ENUM_TAG = 15;
    public static final int IMAGE_SYM_CLASS_MEMBER_OF_ENUM = 16;
    public static final int IMAGE_SYM_CLASS_REGISTER_PARAM = 17;
    public static final int IMAGE_SYM_CLASS_BIT_FIELD = 18;
    public static final int IMAGE_SYM_CLASS_BLOCK = 100;
    public static final int IMAGE_SYM_CLASS_FUNCTION = 101;
    public static final int IMAGE_SYM_CLASS_END_OF_STRUCT = 102;
    public static final int IMAGE_SYM_CLASS_FILE = 103;
    public static final int IMAGE_SYM_CLASS_SECTION = 104;
    public static final int IMAGE_SYM_CLASS_WEAK_EXTERNAL = 105;
    public static final int IMAGE_SYM_CLASS_CLR_TOKEN = 107;

    //debug types
    public static final int IMAGE_DEBUG_TYPE_UNKNOWN = 0;
    public static final int IMAGE_DEBUG_TYPE_COFF = 1;
    public static final int IMAGE_DEBUG_TYPE_CODEVIEW = 2;
    public static final int IMAGE_DEBUG_TYPE_FPO = 3;
    public static final int IMAGE_DEBUG_TYPE_MISC = 4;
    public static final int IMAGE_DEBUG_TYPE_EXCEPTION = 5;
    public static final int IMAGE_DEBUG_TYPE_FIXUP = 6;
    public static final int IMAGE_DEBUG_TYPE_OMAP_TO_SRC = 7;
    public static final int IMAGE_DEBUG_TYPE_OMAP_FROM_SRC = 8;
    public static final int IMAGE_DEBUG_TYPE_BORLAND = 9;
    public static final int IMAGE_DEBUG_TYPE_RESERVED10 = 10;
    public static final int IMAGE_DEBUG_TYPE_CLSID = 11;

    // Used in ThreadLocalStorageTable
    public static final int DLL_PROCESS_ATTACH = 1;
    public static final int DLL_THREAD_ATTACH = 2;
    public static final int DLL_THREAD_DETACH = 3;
    public static final int DLL_PROCESS_DETACH = 0;



    private PEConstants() {}

}
