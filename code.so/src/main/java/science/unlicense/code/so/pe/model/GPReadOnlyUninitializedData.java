

package science.unlicense.code.so.pe.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.code.so.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class GPReadOnlyUninitializedData extends Section {

    public GPReadOnlyUninitializedData(SectionHeader header) {
        super(header, new Chars(".srdata"));
    }

}
