
package science.unlicense.code.so.so;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class ELFConstants {

    private ELFConstants() {}

    public static final byte[] SIGNATURE = new byte[]{0x7F,'E','L','F'};

    //system bits 32 or 64
    public static final int ENC_NONE = 0;
    public static final int ENC_32 = 1;
    public static final int ENC_64 = 2;

    // file endianess
    public static final int END_NONE = 0;
    public static final int END_LSB = 1;
    public static final int END_MSB = 2;

    // Operating system and application binary interface
    public static final int ABI_NONE = 0;
    public static final int ABI_SYSV = 0;
    public static final int ABI_HPUX = 1;
    public static final int ABI_LINUX = 3;
    public static final int ABI_FREEBSD = 4;
    public static final int ABI_NETBSD = 5;
    public static final int ABI_OPENBSD = 6;
    public static final int ABI_ARM = 97;
    public static final int ABI_STANDALONE = 255;

    /** e_type : No file type */
    public static final int ET_NONE = 0;
    /** e_type : Relocatable file */
    public static final int ET_REL = 1;
    /** e_type : Executable file */
    public static final int ET_EXEC = 2;
    /** e_type : Shared object file */
    public static final int ET_DYN = 3;
    /** e_type : Core file */
    public static final int ET_CORE = 4;
    /** e_type : Operating system-specific */
    public static final int ET_LOOS = 0xfe00;
    /** e_type : Operating system-specific */
    public static final int ET_HIOS = 0xfeff;
    /** e_type : Processor-specific */
    public static final int ET_LOPROC = 0xff00;
    /** e_type : Processor-specific */
    public static final int ET_HIPROC = 0xffff;

    /** e_machine : No machine */
    public static final int EM_NONE = 0;
    /** e_machine : AT&T WE 32100 */
    public static final int EM_M32 = 1;
    /** e_machine : SPARC */
    public static final int EM_SPARC = 2;
    /** e_machine : Intel 80386 */
    public static final int EM_386 = 3;
    /** e_machine : Motorola 68000 */
    public static final int EM_68K = 4;
    /** e_machine : Motorola 88000 */
    public static final int EM_88K = 5;
    /** e_machine : Reserved for future use */
    public static final int RESERVED = 6;
    /** e_machine : Intel 80860 */
    public static final int EM_860 = 7;
    /** e_machine : MIPS I Architecture */
    public static final int EM_MIPS = 8;
    // 9 Reserved for future use
    /** e_machine : MIPS RS3000 Little-endian */
    public static final int EM_MIPS_RS3_LE = 10;
    // 11-14 Reserved for future use
    /** e_machine : Hewlett-Packard PA-RISC */
    public static final int EM_PARISC = 15;
    // 16 Reserved for future use
    /** e_machine : Fujitsu VPP500 */
    public static final int EM_VPP500 = 17;
    /** e_machine : Enhanced instruction set SPARC */
    public static final int EM_SPARC32PLUS = 18;
    /** e_machine : Intel 80960 */
    public static final int EM_960 = 19;
    /** e_machine : Power PC */
    public static final int EM_PPC = 20;
    // 21-35 Reserved for future use
    /** e_machine : NEC V800 */
    public static final int EM_V800 = 36;
    /** e_machine : Fujitsu FR20 */
    public static final int EM_FR20 = 37;
    /** e_machine : TRW RH-32 */
    public static final int EM_RH32 = 38;
    /** e_machine : Motorola RCE */
    public static final int EM_RCE = 39;
    /** e_machine : Advanced RISC Machines ARM */
    public static final int EM_ARM = 40;
    /** e_machine : Digital Alpha */
    public static final int EM_ALPHA = 41;
    /** e_machine : Hitachi SH */
    public static final int EM_SH = 42;
    /** e_machine : SPARC Version 9 */
    public static final int EM_SPARCV9 = 43;
    /** e_machine : Siemens Tricore embedded processor */
    public static final int EM_TRICORE = 44;
    /** e_machine : Argonaut RISC Core, Argonaut Technologies Inc. */
    public static final int EM_ARC = 45;
    /** e_machine : Hitachi H8/300 */
    public static final int EM_H8_300 = 46;
    /** e_machine : Hitachi H8/300H */
    public static final int EM_H8_300H = 47;
    /** e_machine : Hitachi H8S */
    public static final int EM_H8S = 48;
    /** e_machine : Hitachi H8/500 */
    public static final int EM_H8_500 = 49;
    /** e_machine : Intel MercedTM Processor */
    public static final int EM_IA_64 = 50;
    /** e_machine : Stanford MIPS-X */
    public static final int EM_MIPS_X = 51;
    /** e_machine : Motorola Coldfire */
    public static final int EM_COLDFIRE= 52;
    /** e_machine : Motorola M68HC12 */
    public static final int EM_68HC12 = 53;

    public static final Chars SECTION_BSS           = Chars.constant(".bss");
    public static final Chars SECTION_COMMENT       = Chars.constant(".comment");
    public static final Chars SECTION_CTORS         = Chars.constant(".ctors");
    public static final Chars SECTION_DATA          = Chars.constant(".data");
    public static final Chars SECTION_DATA1         = Chars.constant(".data1");
    public static final Chars SECTION_DEBUG         = Chars.constant(".debug");
    public static final Chars SECTION_DTORS         = Chars.constant(".dtors");
    public static final Chars SECTION_DYNMAIC       = Chars.constant(".dynamic");
    public static final Chars SECTION_DYNSTR        = Chars.constant(".dynstr");
    public static final Chars SECTION_DYNSYM        = Chars.constant(".dynsym");
    public static final Chars SECTION_FINI          = Chars.constant(".fini");
    public static final Chars SECTION_GNU_VERSION   = Chars.constant(".gnu.version");
    public static final Chars SECTION_GNU_VERSION_D = Chars.constant(".gnu.version_d");
    public static final Chars SECTION_GNU_VERSION_R = Chars.constant(".gnu.version_r");
    public static final Chars SECTION_GOT           = Chars.constant(".got");
    public static final Chars SECTION_HASH          = Chars.constant(".hash");
    public static final Chars SECTION_INIT          = Chars.constant(".init");
    public static final Chars SECTION_INTERP        = Chars.constant(".interp");
    public static final Chars SECTION_LINE          = Chars.constant(".line");
    public static final Chars SECTION_NOTE          = Chars.constant(".note");
    public static final Chars SECTION_NOTE_GNU_STACK= Chars.constant(".note.GNU-stack");
    public static final Chars SECTION_PLT           = Chars.constant(".plt");
    public static final Chars SECTION_RELNAME       = Chars.constant(".relNAME");
    public static final Chars SECTION_RELANAME      = Chars.constant(".relaNAME");
    public static final Chars SECTION_RODATA        = Chars.constant(".rodata");
    public static final Chars SECTION_RODATA1       = Chars.constant(".rodata1");
    public static final Chars SECTION_SHSTRTAB      = Chars.constant(".shstrtab");
    public static final Chars SECTION_STRTAB        = Chars.constant(".strtab");
    public static final Chars SECTION_SYMTAB        = Chars.constant(".symtab");
    public static final Chars SECTION_TEXT          = Chars.constant(".text");

}
