
package science.unlicense.code.so.pe;

import science.unlicense.code.so.pe.model.Section;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;

/**
 * Reader for microsoft portable executable format.
 *
 * http://msdn.microsoft.com/en-us/library/windows/desktop/ms680195(v=vs.85).aspx
 * http://fr.wikipedia.org/wiki/Portable_Executable#En-t.C3.AAte_MZ_sous_MS-DOS
 * http://www.microsoft.com/whdc/system/platform/firmware/PECOFF.mspx
 *
 * @author Johann Sorel
 */
public class PEReader extends AbstractReader{


    public PortableExecutable read() throws IOException{
        final PortableExecutable exec = new PortableExecutable();

        final BacktrackInputStream bs = getInputAsBacktrackStream();
        final DataInputStream ds = new DataInputStream(bs, Endianness.LITTLE_ENDIAN);
        bs.mark();

        //read dos header
        int sign = ds.readShort();
        if (sign != PEConstants.DOS_HEADER) {
            throw new IOException(getInput(), "Missing Dos header.");
        }
        exec.dosHeader = new DosHeader();
        exec.dosHeader.read(ds);

        //read the dos code
        exec.dosCode = new DosCode();
        exec.dosCode.data = ds.readFully(new byte[exec.dosHeader.e_lfanew - DosHeader.BYTE_SIZE - 2]);

        //read pe header
        sign = ds.readInt();
        if (sign != PEConstants.PE_HEADER) {
            throw new IOException(getInput(), "Missing PE header.");
        }
        exec.peHeader = new PEHeader();
        exec.peHeader.read(ds);

        //read section headers
        final int nbSection = exec.peHeader.fileHeader.NumberOfSections;
        exec.sectionHeaders = new SectionHeader[nbSection];
        exec.sections = new Section[nbSection];
        for (int i=0;i<nbSection;i++) {
            exec.sectionHeaders[i] = new SectionHeader();
            exec.sectionHeaders[i].read(ds);
        }

        //read each section
        for (int i=0;i<nbSection;i++) {
            final SectionHeader sh = exec.sectionHeaders[i];
            if (sh.SizeOfRawData<=0) {
                //empty section
                continue;
            }

            bs.rewind();
            final int dataAdr = sh.PointerToRawData;
            ds.skipFully(dataAdr);

            final Section section = Section.getSection(sh);
            section.read(ds);
            exec.sections[i] = section;
        }

        return exec;
    }


}
