

package science.unlicense.code.so.pe.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.code.so.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class GPRelativeInitializedData extends Section {

    public GPRelativeInitializedData(SectionHeader header) {
        super(header, new Chars(".vsdata"));
    }

}
