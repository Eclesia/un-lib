

package science.unlicense.format.swf;

import science.unlicense.format.swf.SWFFormat;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.common.api.Assert;
import science.unlicense.media.api.MediaFormat;
import science.unlicense.media.api.Medias;

/**
 * Test SWF Format declaration.
 *
 * @author Johann Sorel
 */
public class SWFFormatTest {

    @Ignore
    @Test
    public void testFormat(){
        final MediaFormat[] formats = Medias.getFormats();
        for (MediaFormat format : formats){
            if (format instanceof SWFFormat){
                return;
            }
        }

        Assert.fail("SWF Format not found.");
    }

}
