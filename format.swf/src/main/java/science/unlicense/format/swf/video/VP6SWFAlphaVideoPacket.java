
package science.unlicense.format.swf.video;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 * Spec : p.218
 *
 * @author Johann Sorel
 */
public class VP6SWFAlphaVideoPacket implements VideoPacket{

    public int OffsetToAlpha;
    public byte[] Data;
    public byte[] AlphaData;

    public void read(DataInputStream in, int length) throws IOException{
        OffsetToAlpha = readUB(in, 24);
        Data = in.readFully(new byte[OffsetToAlpha]);
        AlphaData = in.readFully(new byte[length-OffsetToAlpha-3]);
    }

    public void write(DataOutputStream out) throws IOException {
        writeUB(out, 24, OffsetToAlpha);
        out.write(Data);
        out.write(AlphaData);
    }

}
