

package science.unlicense.format.swf.action;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SetTarget extends Action {

    /** STRING */
    public Chars TargetName;

    public SetTarget() {
        super(0x8B);
    }

    public void read(DataInputStream ds) throws IOException {
        super.read(ds);
        TargetName = readChars(ds);
    }

}
