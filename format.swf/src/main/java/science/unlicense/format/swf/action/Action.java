

package science.unlicense.format.swf.action;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class Action {

    public final int code;

    public int Length = 0;

    protected Action(int code) {
        this.code = code;
    }

    public void read(DataInputStream ds) throws IOException {
        if (code>=0x80){
            Length = ds.readUShort();
        }
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new IOException(ds, "Not implemented yet");
    }

}
