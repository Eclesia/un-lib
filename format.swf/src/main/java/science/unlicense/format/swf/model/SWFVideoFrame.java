

package science.unlicense.format.swf.model;

import science.unlicense.encoding.api.io.ArrayInputStream;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.swf.video.H263VideoPacket;
import science.unlicense.format.swf.video.ScreenVideoPacket;
import science.unlicense.format.swf.video.VP6SWFAlphaVideoPacket;
import science.unlicense.format.swf.video.VP6SWFVideoPacket;
import science.unlicense.format.swf.video.VideoPacket;

/**
 *
 * @author Johann Sorel
 */
public class SWFVideoFrame extends SWFTag {

    /** UI16 */
    public int StreamID;
    /** UI16 */
    public int FrameNum;
    /**
     * CodecID = 2 H263VIDEOPACKET
     * CodecID = 3 SCREENVIDEOPACKET
     * CodecID = 4 VP6SWFVIDEOPACKET
     * CodecID = 5 VP6SWFALPHAVIDEOPACKET
     * CodecID = 6 SCREENV2VIDEOPACKET
     */
    public byte[] VideoData;

    public void read(DataInputStream ds) throws IOException {
        StreamID = ds.readUShort();
        FrameNum = ds.readUShort();
        VideoData = ds.readFully(new byte[remainingBytes(ds)]);
    }

    public void write(DataOutputStream ds) throws IOException {
        super.write(ds);
        ds.writeUShort(StreamID);
        ds.writeUShort(FrameNum);
        ds.write(VideoData);
    }

    public VideoPacket readPacket(SWFDefineVideoStream define) throws IOException{
        final VideoPacket packet;
        switch(define.CodecID){
            case 2 : packet = new H263VideoPacket(); break;
            case 3 : packet = new ScreenVideoPacket(); break;
            case 4 : packet = new VP6SWFVideoPacket(); break;
            case 5 : packet = new VP6SWFAlphaVideoPacket(); break;
            default : throw new IOException(null, "Unexpected codec "+define.CodecID);
        }
        packet.read(new DataInputStream(new ArrayInputStream(VideoData)), VideoData.length);
        return packet;
    }


}
