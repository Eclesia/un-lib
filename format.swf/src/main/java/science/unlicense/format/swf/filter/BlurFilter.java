

package science.unlicense.format.swf.filter;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class BlurFilter extends Filter {

    /** FIXED */
    public float BlurX;
    /** FIXED */
    public float BlurY;
    /** UB[5] */
    public int Passes;


    public void read(DataInputStream ds) throws IOException {

        BlurX = readFixed32(ds);
        BlurY = readFixed32(ds);
        Passes = ds.readBits(5);
        ds.readBits(3);
    }


}
