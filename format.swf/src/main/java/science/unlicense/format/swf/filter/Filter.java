

package science.unlicense.format.swf.filter;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class Filter {

    public void read(DataInputStream ds) throws IOException {
        throw new IOException(ds, "Not implemented yet");
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new IOException(ds, "Not implemented yet");
    }

}
