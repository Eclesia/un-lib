
package science.unlicense.format.swf.video;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 * Spec : p.217
 *
 * @author Johann Sorel
 */
public class VP6FLVVideoPacket implements VideoPacket {

    public int HorizontalAdjustment;
    public int VerticalAdjustment;
    public byte[] Data;

    public void read(DataInputStream in, int length) throws IOException{
        HorizontalAdjustment = readUB(in, 4);
        VerticalAdjustment = readUB(in, 4);
        Data = in.readFully(new byte[length-1]);
    }

    public void write(DataOutputStream out) throws IOException {
        writeUB(out, 4, HorizontalAdjustment);
        writeUB(out, 4, VerticalAdjustment);
        out.write(Data);
    }

}
