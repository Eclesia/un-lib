
package science.unlicense.format.swf.video;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 * Spec : p.217
 *
 * @author Johann Sorel
 */
public class VP6FLVAlphaVideoPacket implements VideoPacket {

    public int HorizontalAdjustment;
    public int VerticalAdjustment;
    public int OffsetToAlpha;
    public byte[] Data;
    public byte[] AlphaData;

    public void read(DataInputStream in, int length) throws IOException{
        HorizontalAdjustment = readUB(in, 4);
        VerticalAdjustment = readUB(in, 4);
        OffsetToAlpha = readUB(in, 24);
        Data = in.readFully(new byte[OffsetToAlpha]);
        AlphaData = in.readFully(new byte[length-OffsetToAlpha-4]);
    }

    public void write(DataOutputStream out) throws IOException {
        writeUB(out, 4, HorizontalAdjustment);
        writeUB(out, 4, VerticalAdjustment);
        writeUB(out, 24, OffsetToAlpha);
        out.write(Data);
        out.write(AlphaData);
    }

}
