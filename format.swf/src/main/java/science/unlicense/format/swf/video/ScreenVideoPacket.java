
package science.unlicense.format.swf.video;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Spec : p.211
 *
 * @author Johann Sorel
 */
public class ScreenVideoPacket implements VideoPacket{

    public void read(DataInputStream in, int length) throws IOException{
        throw new UnimplementedException("TODO");
    }

    public void write(DataOutputStream out) throws IOException {
        throw new UnimplementedException("TODO");
    }

}
