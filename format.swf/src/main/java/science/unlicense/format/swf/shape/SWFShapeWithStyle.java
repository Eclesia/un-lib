
package science.unlicense.format.swf.shape;

import science.unlicense.common.api.CObject;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.tree.Nodes;
import science.unlicense.geometry.impl.Path;
import science.unlicense.format.swf.SWFUtilities;
import science.unlicense.geometry.api.PlanarGeometry;

/**
 *
 * @author Johann Sorel
 */
public class SWFShapeWithStyle extends CObject{

    public SWFFillStyleArray fillStyles;
    public SWFLineStyleArray lineStyles;
    /** UB[4] */
    public int numFillBits;
    /** UB[4] */
    public int numLineBits;
    /** SHAPERECORD[N] */
    public Sequence shapeRecords = new ArraySequence();

    /**
     * Draft rebuild geometry from shape records.
     *
     * @return
     */
    public PlanarGeometry buildGeometry(){

        final Path path = new Path();

        for (int i=0,n=shapeRecords.getSize();i<n;i++){
            final Object cdt = shapeRecords.get(i);
            if (cdt instanceof SWFStyleChangeRecord){
                final SWFStyleChangeRecord c = (SWFStyleChangeRecord) cdt;
                if (c.StateMoveTo==1){
                    path.appendMoveTo(c.MoveDeltaX, c.MoveDeltaY);
                }

            } else if (cdt instanceof SWFCurvedEdgeRecord){
                final SWFCurvedEdgeRecord c = (SWFCurvedEdgeRecord) cdt;
                path.appendQuadToRelative(
                        c.controlDeltaX, c.controlDeltaY,
                        c.anchorDeltaX, c.anchorDeltaY);

            } else if (cdt instanceof SWFStraightEdgeRecord){
                final SWFStraightEdgeRecord c = (SWFStraightEdgeRecord) cdt;
                path.appendLineToRelative(c.deltaX, c.deltaY);

            } else {
                throw new RuntimeException("Unexpected type "+cdt);
            }
        }

        return path;
    }

    public void read(DataInputStream ds, int shapeversion) throws IOException {
        fillStyles = new SWFFillStyleArray();
        fillStyles.read(ds, shapeversion);
        lineStyles = new SWFLineStyleArray();
        lineStyles.read(ds, shapeversion);
        numFillBits = SWFUtilities.readUB(ds, 4);
        numLineBits = SWFUtilities.readUB(ds, 4);

        while (true){
            final int flag0 = SWFUtilities.readUB(ds, 1);

            if (flag0==0){
                final int flags = SWFUtilities.readUB(ds, 5);
                if (flags==0){
                    //end
                    break;
                } else {
                    //style change
                    final SWFStyleChangeRecord rec = new SWFStyleChangeRecord(shapeversion);
                    rec.read(ds, flags, numFillBits, numLineBits);
                    shapeRecords.add(rec);
                    if (rec.StateNewStyles==1){
                        //this block changes the number of the next blocks bits read
                        numFillBits = rec.NumFillBits;
                        numLineBits = rec.NumLineBits;
                    }
                }
            } else {
                final int flag1 = SWFUtilities.readUB(ds, 1);
                if (flag1==1){
                    final SWFStraightEdgeRecord rec = new SWFStraightEdgeRecord();
                    rec.read(ds);
                    shapeRecords.add(rec);
                } else {
                    final SWFCurvedEdgeRecord rec = new SWFCurvedEdgeRecord();
                    rec.read(ds);
                    shapeRecords.add(rec);
                }
            }
        }

    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(this.getClass().getSimpleName());
        cb.append(Nodes.toChars(new Chars("shapes"), shapeRecords));
        return cb.toChars();
    }

}
