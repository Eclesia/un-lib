

package science.unlicense.format.swf.action;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class WaitForFrame extends Action {

    /** UI16 */
    public int Frame;
    /** UI8 */
    public int SkipCount;

    public WaitForFrame() {
        super(0x8A);
    }

    public void read(DataInputStream ds) throws IOException {
        super.read(ds);
        Frame = ds.readUShort();
        SkipCount = ds.readUByte();
    }

}
