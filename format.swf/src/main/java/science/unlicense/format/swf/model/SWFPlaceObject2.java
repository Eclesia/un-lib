

package science.unlicense.format.swf.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.format.swf.color.ColorTransform;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFPlaceObject2 extends SWFTag {

    public static final class ClipActionRecord{
        public SWFClipEventFlags EventFlags;
        /** UI32 */
        public int ActionRecordSize;
        /** UI8 */
        public byte KeyCode;
        /** ACTIONRECORD */
        public ClipActionRecord[] Actions;
    }

    /** UB[1] */
    public int PlaceFlagHasClipActions;
    /** UB[1] */
    public int PlaceFlagHasClipDepth;
    /** UB[1] */
    public int PlaceFlagHasName;
    /** UB[1] */
    public int PlaceFlagHasRatio;
    /** UB[1] */
    public int PlaceFlagHasColorTransform;
    /** UB[1] */
    public int PlaceFlagHasMatrix;
    /** UB[1] */
    public int PlaceFlagHasCharacter;
    /** UB[1] */
    public int PlaceFlagMove;

    /** UI16 */
    public int Depth;
    /** UI16 */
    public int CharacterId;
    /** MATRIX */
    public Matrix3x3 matrix;
    /** CXFORM */
    public ColorTransform ColorTransform;
    /** UI16 */
    public int Ratio;
    /** String */
    public Chars Name;
    /** UI16 */
    public int ClipDepth;
    /** UI16 */
    public int ClipActions;

    //for clip
    /** UI16 */
    public int Reserved;
    /** CLIPEVENTFLAGS */
    public SWFClipEventFlags AllEventFlags;
    /** CLIPACTIONRECORD */
    public int ClipActionRecords[];
    /**
     * UI16. If SWF version <= 5
     * UI32. If SWF version >= 6
     */
    public int ClipActionEndFlag;


    public void read(DataInputStream ds) throws IOException {
        PlaceFlagHasClipActions     = readUB(ds, 1);
        PlaceFlagHasClipDepth       = readUB(ds, 1);
        PlaceFlagHasName            = readUB(ds, 1);
        PlaceFlagHasRatio           = readUB(ds, 1);
        PlaceFlagHasColorTransform  = readUB(ds, 1);
        PlaceFlagHasMatrix          = readUB(ds, 1);
        PlaceFlagHasCharacter       = readUB(ds, 1);
        PlaceFlagMove               = readUB(ds, 1);
        Depth                       = ds.readUShort();
        if (PlaceFlagHasCharacter==1)        CharacterId     = ds.readUShort();
        if (PlaceFlagHasMatrix==1)           matrix          = readMatrix(ds);
        if (PlaceFlagHasColorTransform==1)   ColorTransform  = readRGBATransform(ds);
        if (PlaceFlagHasRatio==1)            Ratio           = ds.readUShort();
        if (PlaceFlagHasName==1)             Name            = readChars(ds);
        if (PlaceFlagHasClipDepth==1)        ClipDepth       = ds.readUShort();
        if (PlaceFlagHasClipActions==1){
            Reserved = ds.readUShort();
            AllEventFlags = readClipEvents(ds);
            //TODO how many events ??
        }

    }

    public void write(DataOutputStream ds) throws IOException {
        super.write(ds);
    }

}
