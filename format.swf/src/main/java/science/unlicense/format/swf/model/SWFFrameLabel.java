

package science.unlicense.format.swf.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFFrameLabel extends SWFTag {

    /** STRING */
    public Chars Name;

    public void read(DataInputStream ds) throws IOException {
        Name = readChars(ds);
    }



}
