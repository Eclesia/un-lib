
package science.unlicense.format.swf.vm;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;

/**
 * Flash virtual machine.
 * TODO : uncomplete
 *
 * @author Johann Sorel
 */
public class SWFVM {

    private final Dictionary dictionnary = new HashDictionary();
    private final Sequence displayList = new ArraySequence();

    public SWFVM() {
    }

    public Dictionary getDictionnary() {
        return dictionnary;
    }

    public Sequence getDisplayList() {
        return displayList;
    }

}
