

package science.unlicense.format.swf.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFMetaData extends SWFTag {

    /** STRING */
    public Chars metadata;

    public void read(DataInputStream ds) throws IOException {
        metadata = readChars(ds);
    }

    public Chars toChars() {
        return new Chars(getClass().getSimpleName()).concat(metadata);
    }

}
