

package science.unlicense.format.swf.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SWFRemoveObject extends SWFTag {

    /** UI16 */
    public int CharacterId;
    /** UI16 */
    public int Depth;

    public void read(DataInputStream ds) throws IOException {
        CharacterId = ds.readUShort();
        Depth = ds.readUShort();
    }

}
