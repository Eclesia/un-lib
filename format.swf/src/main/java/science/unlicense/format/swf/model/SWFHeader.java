
package science.unlicense.format.swf.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.swf.SWFConstants;
import science.unlicense.format.swf.SWFObject;

/**
 *
 * @author Johann Sorel
 */
public class SWFHeader extends SWFObject {

    public Chars signature;
    public int version;
    public long fileLength;

    public void read(final DataInputStream ds) throws IOException{
        signature = new Chars(ds.readFully(new byte[3]));
        if (SWFConstants.SIGNATURE_FWS.equals(signature)){
        } else if (SWFConstants.SIGNATURE_CWS.equals(signature)){
        } else if (SWFConstants.SIGNATURE_ZWS.equals(signature)){
        } else {
            throw new IOException(ds, "File is not a valid SWF.");
        }

        version = ds.readUByte();
        fileLength = ds.readUInt();
    }

    public void write(final DataOutputStream ds) throws IOException{
        ds.write(signature.toBytes());
        ds.writeUByte(version);
        ds.writeUInt(fileLength);
    }

}
