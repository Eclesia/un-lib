
package science.unlicense.format.swf.model;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.swf.SWFObject;

/**
 *
 * @author Johann Sorel
 */
public class SWFTag extends SWFObject{

    public int type;
    /**
     * byte lenght of the tag not including recordheader
     */
    public long length;
    public boolean longTag;
    /**
     * Offset of the tag in the file after the recordheader
     */
    public long offset;

    /**
     * Read tag content, this do not read the tag header.
     *
     * @param ds
     * @throws IOException
     */
    public void read(DataInputStream ds) throws IOException {
        ds.skipFully(length);
    }

    /**
     * Write tag content, this do not write the tag header.
     *
     * @param ds
     * @throws IOException
     */
    public void write(DataOutputStream ds) throws IOException {
        throw new IOException(ds, "Not implemented yet.");
    }

    protected int remainingBytes(DataInputStream ds){
        if (longTag){
            return (int) (length - (ds.getByteOffset() - offset));
        } else {
            return (int) (length - (ds.getByteOffset() - offset));
        }
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(this.getClass().getSimpleName()).append(':');
        cb.append(CObjects.toChars(length));
        return cb.toChars();
    }

}
