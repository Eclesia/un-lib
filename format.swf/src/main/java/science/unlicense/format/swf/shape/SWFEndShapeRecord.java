
package science.unlicense.format.swf.shape;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.swf.SWFObject;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFEndShapeRecord extends SWFObject {

    /** UB[1] */
    public int typeFlag;
    /** UB[5] */
    public int endOfShape;

    public void read(DataInputStream ds) throws IOException {
        typeFlag = readUB(ds, 1);
        endOfShape = readUB(ds, 5);
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
