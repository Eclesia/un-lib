
package science.unlicense.format.swf.video;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Spec : p.218
 *
 * @author Johann Sorel
 */
public class VP6SWFVideoPacket implements VideoPacket{

    public byte[] Data;

    public void read(DataInputStream in, int length) throws IOException{
        Data = in.readFully(new byte[length]);
    }

    public void write(DataOutputStream out) throws IOException {
        out.write(Data);
    }

}
