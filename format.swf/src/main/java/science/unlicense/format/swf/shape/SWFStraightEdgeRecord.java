
package science.unlicense.format.swf.shape;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.readUB;

/**
 *
 * @author Johann Sorel
 */
public class SWFStraightEdgeRecord {

    /** UB[4]*/
    public int numBits;
    /** UB[1] */
    public int generalLineFlag;
    /** 0 or SB[1] */
    public int vertLineFlag;
    /** 0 or SB[NumBits+2] */
    public int deltaX;
    /** 0 or SB[NumBits+2] */
    public int deltaY;

    public void read(DataInputStream ds) throws IOException {
        numBits = readUB(ds, 4);
        generalLineFlag = readUB(ds, 1);
        if (generalLineFlag==0) vertLineFlag = readUB(ds, 1);
        if (generalLineFlag==1 || vertLineFlag==0) deltaX = ds.readSignedBits(numBits+2);
        if (generalLineFlag==1 || vertLineFlag==1) deltaY = ds.readSignedBits(numBits+2);
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public String toString() {
        return "SWFStraightEdgeRecord "+deltaX+"  "+deltaY;
    }

}
