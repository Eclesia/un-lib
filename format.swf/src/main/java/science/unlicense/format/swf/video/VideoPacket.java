
package science.unlicense.format.swf.video;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * Video packets are contained in the SWFVideoFrame tag.
 * The type of video packet is defined in the SWFDefineVideoStream.
 *
 * @author Johann Sorel
 */
public interface VideoPacket {

    /**
     * Read video packet.
     *
     * @param in input
     * @param frameLength total frame length
     * @throws science.unlicense.encoding.api.io.IOException
     */
    void read(DataInputStream in, int frameLength) throws IOException;

    /**
     * Write video packet.
     *
     * @param out output
     * @throws science.unlicense.encoding.api.io.IOException
     */
    void write(DataOutputStream out) throws IOException;

}
