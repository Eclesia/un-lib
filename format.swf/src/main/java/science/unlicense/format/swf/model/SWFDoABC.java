

package science.unlicense.format.swf.model;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFDoABC extends SWFTag {

    /** UI32 */
    public int Flags;
    /** STRING */
    public Chars Name;
    /** BYTE[] */
    public byte[] ABCData;


    public void read(DataInputStream ds) throws IOException {
        Flags = ds.readInt();
        Name = readChars(ds);
        ABCData = ds.readFully(new byte[remainingBytes(ds)]);
    }

    public void write(DataOutputStream ds) throws IOException {
        super.write(ds);
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(this.getClass().getSimpleName()).append(':');
        cb.append(Name);
        return cb.toChars();
    }

}
