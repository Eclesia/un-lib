
package science.unlicense.format.swf.shape;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.swf.SWFObject;

/**
 *
 * @author Johann Sorel
 */
public class SWFShapeRecord extends SWFObject {

    public void read(DataInputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
