
package science.unlicense.format.swf;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.DefaultFormat;
import science.unlicense.media.api.Media;

/**
 * Specification :
 * http://www.adobe.com/devnet/swf.html
 *
 * @author Johann Sorel
 */
public class SWFFormat extends DefaultFormat {

    public static final SWFFormat INSTANCE = new SWFFormat();

    public SWFFormat() {
        super(new Chars("swf"));
        shortName = new Chars("SWF");
        longName = new Chars("SWF");
        mimeTypes.add(new Chars("application/vnd.adobe.flash-movie"));
        extensions.add(new Chars("swf"));
        resourceTypes.add(Media.class);
    }

    public boolean supportReading() {
        return true;
    }

    public SWFStore open(Object input) throws IOException {
        return new SWFStore((Path) input);
    }

}
