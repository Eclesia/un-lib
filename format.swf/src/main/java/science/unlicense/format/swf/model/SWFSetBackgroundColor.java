

package science.unlicense.format.swf.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.color.Color;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFSetBackgroundColor extends SWFTag {

    /** RGB */
    public Color backgroundColor;

    public void read(DataInputStream ds) throws IOException {
        backgroundColor = readRGB(ds);
    }

    public void write(DataOutputStream ds) throws IOException {
        writeRGB(ds, backgroundColor);
    }

    public Chars toChars() {
        return new Chars(getClass().getSimpleName()).concat(backgroundColor.toChars());
    }

}
