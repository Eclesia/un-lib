

package science.unlicense.format.swf.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFImportAssets extends SWFTag {

    /** URL */
    public Chars url;
    /** UI16 */
    public int Count;
    /** Map of <Tag,Name> UI16,String. */
    public final Dictionary tags = new HashDictionary();

    public void read(DataInputStream ds) throws IOException {
        url = readChars(ds);
        Count = ds.readUShort();
        for (int i=0;i<Count;i++){
            int tag = ds.readUShort();
            Chars name = readChars(ds);
            tags.add(tag, name);
        }
    }

}
