
package science.unlicense.format.swf.shape;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.swf.SWFObject;
import science.unlicense.format.swf.SWFUtilities;

/**
 *
 * @author Johann Sorel
 */
public class SWFShape extends SWFObject {

    /** UB[4] */
    public int NumFillBits;
    /** UB[4] */
    public int NumLineBits;
    /** SHAPERECORD[N] */
    public Sequence shapeRecords = new ArraySequence();

    public void read(DataInputStream ds) throws IOException {
        NumFillBits = SWFUtilities.readUB(ds, 4);
        NumLineBits = SWFUtilities.readUB(ds, 4);
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
