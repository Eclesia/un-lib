

package science.unlicense.format.swf.filter;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.color.Color;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class GradientGlowFilter extends Filter {

    /** UI8 */
    public int NumColors;
    /** RGBA[NumColors] */
    public Color[] GradientColors;
    /** UI8[NumColors] */
    public int[] GradientRatio;
    /** FIXED */
    public float BlurX ;
    /** FIXED */
    public float BlurY;
    /** FIXED8 */
    public float Strength;
    /** UB[1] */
    public int InnerGlow;
    /** UB[1] */
    public int Knockout;
    /** UB[1] */
    public int CompositeSource;
    /** UB[5] */
    public int Passes;



    public void read(DataInputStream ds) throws IOException {
        NumColors = ds.readUByte();
        GradientColors = new Color[NumColors];
        for (int i=0;i<NumColors;i++){
            GradientColors[i] = readRGBA(ds);
        }
        GradientRatio = ds.readUByte(NumColors);
        BlurX = readFixed32(ds);
        BlurY = readFixed32(ds);
        Strength = readFB(ds, 8);
        InnerGlow = ds.readBits(1);
        Knockout = ds.readBits(1);
        CompositeSource = ds.readBits(1);
        Passes = ds.readBits(5);
    }


}
