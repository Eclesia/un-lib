

package science.unlicense.format.swf.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFProtect extends SWFTag {

    public void read(DataInputStream ds) throws IOException {
        super.read(ds);
    }

    public void write(DataOutputStream ds) throws IOException {
        super.write(ds);
    }

}
