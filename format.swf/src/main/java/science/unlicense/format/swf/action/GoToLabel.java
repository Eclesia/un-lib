

package science.unlicense.format.swf.action;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class GoToLabel extends Action {

    /** STRING */
    public Chars Label;

    public GoToLabel() {
        super(0x8C);
    }

    public void read(DataInputStream ds) throws IOException {
        super.read(ds);
        Label = readChars(ds);
    }

}
