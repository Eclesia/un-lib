
package science.unlicense.format.swf.shape;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFCurvedEdgeRecord {

    /** UB[4]*/
    public int numBits;
    /** SB[NumBits+2]*/
    public int controlDeltaX;
    /** SB[NumBits+2]*/
    public int controlDeltaY;
    /** SB[NumBits+2]*/
    public int anchorDeltaX;
    /** SB[NumBits+2]*/
    public int anchorDeltaY;

    public void read(DataInputStream ds) throws IOException {
        numBits = readUB(ds, 4);
        controlDeltaX = ds.readSignedBits(numBits+2);
        controlDeltaY = ds.readSignedBits(numBits+2);
        anchorDeltaX = ds.readSignedBits(numBits+2);
        anchorDeltaY = ds.readSignedBits(numBits+2);
    }

    public void write(DataOutputStream ds) throws IOException {
        writeUB(ds, 1, 1);
        writeUB(ds, 1, 0);
        writeUB(ds, 4, numBits);
        writeSB(ds, numBits+2, controlDeltaX);
        writeSB(ds, numBits+2, controlDeltaY);
        writeSB(ds, numBits+2, anchorDeltaX);
        writeSB(ds, numBits+2, anchorDeltaY);
    }

    @Override
    public String toString() {
        return "SWFCurvedEdgeRecord "+controlDeltaX+"  "+controlDeltaY+"  "+anchorDeltaX+"  "+anchorDeltaY;
    }

}
