
package science.unlicense.format.swf.shape;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.swf.SWFUtilities;

/**
 *
 * @author Johann Sorel
 */
public class SWFLineStyle2 extends SWFLineStyle{

    public static final int CAP_ROUND = 0;
    public static final int CAP_NONE = 1;
    public static final int CAP_SQUARE = 2;

    public static final int JOIN_ROUND = 0;
    public static final int JOIN_BEVEL = 1;
    public static final int JOIN_MITER = 2;

    /** UB[2] */
    public int startCapStyle;
    /** UB[2] */
    public int joinStyle;
    /** UB[2] */
    public int hasFillFlag;
    /** UB[2] */
    public int noHScaleFlag;
    /** UB[2] */
    public int noVScaleFlag;
    /** UB[2] */
    public int pixelHintFlag;
    /** UB[2] */
    public int reserved;
    /** UB[2] */
    public int noClose;
    /** UB[2] */
    public int endCapStyle;
    /** UB[2] */
    public int miterLimitFactor;

    public SWFFillStyle fillType;

    public void read(DataInputStream ds, int shapeVersion) throws IOException {
        width = ds.readUShort();
        startCapStyle = SWFUtilities.readUB(ds, 2);
        joinStyle = SWFUtilities.readUB(ds, 2);
        hasFillFlag = SWFUtilities.readUB(ds, 1);
        noHScaleFlag = SWFUtilities.readUB(ds, 1);
        noVScaleFlag = SWFUtilities.readUB(ds, 1);
        pixelHintFlag = SWFUtilities.readUB(ds, 1);
        reserved = SWFUtilities.readUB(ds, 5);
        noClose = SWFUtilities.readUB(ds, 1);
        endCapStyle = SWFUtilities.readUB(ds, 2);
        if (joinStyle==JOIN_MITER) miterLimitFactor = ds.readUShort();
        if (hasFillFlag==0){
            color = SWFUtilities.readRGBA(ds);
        }
        if (hasFillFlag==1){
            fillType = new SWFFillStyle();
            fillType.read(ds, shapeVersion);
        }
    }

    public void write(DataOutputStream ds, int shapeVersion) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
