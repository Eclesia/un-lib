

package science.unlicense.format.swf.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFEnableDebugger2 extends SWFTag {

    /** UI16 */
    public int reserved;
    /** STRING */
    public Chars password;

    public void read(DataInputStream ds) throws IOException {
        reserved = ds.readUShort();
        password = readChars(ds);
    }

    public void write(DataOutputStream ds) throws IOException {
        super.write(ds);
        ds.writeUShort(reserved);
        writeChars(ds, password);
    }

}
