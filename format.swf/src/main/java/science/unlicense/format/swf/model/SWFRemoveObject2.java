

package science.unlicense.format.swf.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SWFRemoveObject2 extends SWFTag {

    /** UI16 */
    public int Depth;

    public void read(DataInputStream ds) throws IOException {
        Depth = ds.readUShort();
    }

}
