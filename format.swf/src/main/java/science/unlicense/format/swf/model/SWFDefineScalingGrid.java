

package science.unlicense.format.swf.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.impl.Rectangle;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFDefineScalingGrid extends SWFTag {

    /** UI16 */
    public int CharacterId;
    /** RECT */
    public Rectangle Splitter;

    public void read(DataInputStream ds) throws IOException {
        CharacterId = ds.readUShort();
        Splitter = readRectangle(ds);
    }


}
