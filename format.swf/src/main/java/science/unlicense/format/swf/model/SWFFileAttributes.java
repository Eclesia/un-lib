

package science.unlicense.format.swf.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.common.api.model.tree.Nodes;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFFileAttributes extends SWFTag {

    /** UB[1] */
    public int Reserved0;
    /** UB[1] */
    public int UseDirectBlit;
    /** UB[1] */
    public int UseGPU;
    /** UB[1] */
    public int HasMetadata;
    /** UB[1] */
    public int ActionScript3;
    /** UB[2] */
    public int Reserved1;
    /** UB[1] */
    public int UseNetwork;
    /** UB[24] */
    public int Reserved2;




    // TODO : there are 2 FileAttributes defined in the spec
    //Don't know yet when this one is used
//    /** UB[3] */
//    public int Reserved0;
//    /** UB[1] */
//    public int HasMetaData;
//    /** UB[1] */
//    public int SWFFlagsAS3;
//    /** UB[1] */
//    public int SWFFlagsNoCrossDomainCache;
//    /** UB[1] */
//    public int Reserved1;
//    /** UB[1] */
//    public int SWFFlagsUseNetwork;


    public void read(DataInputStream ds) throws IOException {
        Reserved0       = readUB(ds, 1);
        UseDirectBlit   = readUB(ds, 1);
        UseGPU          = readUB(ds, 1);
        HasMetadata     = readUB(ds, 1);
        ActionScript3   = readUB(ds, 1);
        Reserved1       = readUB(ds, 2);
        UseNetwork      = readUB(ds, 1);
        Reserved2       = readUB(ds, 24);

//        Reserved0 = readUB(ds, 3);
//        HasMetaData = readUB(ds, 1);
//        SWFFlagsAS3 = readUB(ds, 1);
//        SWFFlagsNoCrossDomainCache = readUB(ds, 1);
//        Reserved1 = readUB(ds, 1);
//        SWFFlagsUseNetwork = readUB(ds, 1);
    }

    public void write(DataOutputStream ds) throws IOException {
        writeUB(ds, 1, Reserved0);
        writeUB(ds, 1, UseDirectBlit);
        writeUB(ds, 1, UseGPU);
        writeUB(ds, 1, HasMetadata);
        writeUB(ds, 1, ActionScript3);
        writeUB(ds, 2, Reserved1);
        writeUB(ds, 1, UseNetwork);
        writeUB(ds, 24, Reserved2);

//        writeUB(ds, 3, Reserved0);
//        writeUB(ds, 1, HasMetaData);
//        writeUB(ds, 1, SWFFlagsAS3);
//        writeUB(ds, 1, SWFFlagsNoCrossDomainCache);
//        writeUB(ds, 1, Reserved1);
//        writeUB(ds, 1, SWFFlagsUseNetwork);
    }

    public Chars toChars() {
        return Nodes.toChars(new Chars(getClass().getSimpleName()), new Object[]{
            new Chars("UseDirectBlit"+UseDirectBlit),
            new Chars("UseGPU:"+UseGPU),
            new Chars("HasMetadata:"+HasMetadata),
            new Chars("ActionScript3:"+ActionScript3),
            new Chars("UseNetwork:"+UseNetwork)
        });
    }

}
