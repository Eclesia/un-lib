

package science.unlicense.format.swf.model;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.impl.Rectangle;
import static science.unlicense.format.swf.SWFUtilities.*;
import science.unlicense.format.swf.shape.SWFShapeWithStyle;

/**
 *
 * @author Johann Sorel
 */
public class SWFDefineShape extends SWFTag {

    /** UInt16 */
    public int shapeId;
    /** RECT */
    public Rectangle bounds;
    /** SHAPEWITHSTYLE */
    public SWFShapeWithStyle shape;

    public void read(DataInputStream ds) throws IOException {
        shapeId = ds.readUShort();
        bounds = readRectangle(ds);
        shape = new SWFShapeWithStyle();
        int version = 0;
        //if (this instanceof SWFDefineShape4) version = 4;
        if (this instanceof SWFDefineShape3) version = 3;
        else if (this instanceof SWFDefineShape2) version = 2;
        else if (this instanceof SWFDefineShape) version = 1;

        shape.read(ds, version);
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUShort(shapeId);
        writeRectangle(ds, bounds);
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(this.getClass().getSimpleName()).append('\n');
        cb.append(CObjects.toChars(shape));
        return cb.toChars();
    }

}
