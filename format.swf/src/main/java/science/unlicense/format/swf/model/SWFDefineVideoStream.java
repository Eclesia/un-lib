

package science.unlicense.format.swf.model;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import static science.unlicense.format.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFDefineVideoStream extends SWFTag {

    /** UI16 */
    public int CharacterID;
    /** UI16 */
    public int NumFrames;
    /** UI16 */
    public int Width;
    /** UI16 */
    public int Height;
    /** UB[4] */
    public int VideoFlagsReserved;
    /**
     * UB[3]
     * 000 = use VIDEOPACKET value
     * 001 = off
     * 010 = Level 1 (Fast deblocking filter)
     * 011 = Level 2 (VP6 only, better deblocking filter)
     * 100 = Level 3 (VP6 only, better deblocking plus fast deringing filter)
     * 101 = Level 4 (VP6 only, better deblocking plus better deringing filter)
     * 110 = Reserved
     * 111 = Reserved
     */
    public int VideoFlagsDeblocking;
    /**
     * UB[1]
     * 0 = smoothing off
     * 1 = smoothing on
     */
    public int VideoFlagsSmoothing;
    /**
     * UI8
     * 2 = Sorenson H.263
     * 3 = Screen video (SWF 7 and later only)
     * 4 = VP6 (SWF 8 and later only)
     * 5 = VP6 video with alpha channel (SWF 8 and later only)
     */
    public int CodecID;

    public void read(DataInputStream ds) throws IOException {
        CharacterID             = ds.readUShort();
        NumFrames               = ds.readUShort();
        Width                   = ds.readUShort();
        Height                  = ds.readUShort();
        VideoFlagsReserved      = readUB(ds, 4);
        VideoFlagsDeblocking    = readUB(ds, 3);
        VideoFlagsSmoothing     = readUB(ds, 1);
        CodecID                 = ds.readUByte();
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUShort(CharacterID);
        ds.writeUShort(NumFrames);
        ds.writeUShort(Width);
        ds.writeUShort(Height);
        writeUB(ds, 4, VideoFlagsReserved);
        writeUB(ds, 3, VideoFlagsDeblocking);
        writeUB(ds, 1, VideoFlagsSmoothing);
        ds.writeUByte(CodecID);
    }

}
