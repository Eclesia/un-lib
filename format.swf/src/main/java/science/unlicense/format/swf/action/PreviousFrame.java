

package science.unlicense.format.swf.action;

/**
 *
 * @author Johann Sorel
 */
public class PreviousFrame extends Action {

    public PreviousFrame() {
        super(0x05);
    }

}
