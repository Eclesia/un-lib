

package science.unlicense.format.swf.action;

/**
 *
 * @author Johann Sorel
 */
public class Stop extends Action {

    public Stop() {
        super(0x07);
    }

}
