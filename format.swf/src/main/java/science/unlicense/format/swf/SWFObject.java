
package science.unlicense.format.swf;

import science.unlicense.common.api.CObject;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 * @author Johann Sorel
 */
public abstract class SWFObject extends CObject{

    public abstract void read(DataInputStream ds) throws IOException;

    public abstract void write(DataOutputStream ds) throws IOException;
}
