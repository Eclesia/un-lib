
package science.unlicense.format.cesium;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.api.AbstractModel3DFormat;

/**
 *
 * @author Johann Sorel
 */
public class QMeshFormat extends AbstractModel3DFormat {

    public static final QMeshFormat INSTANCE = new QMeshFormat();

    private QMeshFormat() {
        super(new Chars("qmesh"));
        shortName = new Chars("qmesh");
        longName = new Chars("Quantized mesh");
        extensions.add(new Chars("qmesh"));
    }

    @Override
    public Store open(Object input) throws IOException {
        return new QMeshStore(input);
    }

}
