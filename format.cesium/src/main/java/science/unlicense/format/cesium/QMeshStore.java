
package science.unlicense.format.cesium;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Float64;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.api.AbstractModel3DStore;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * Specification :
 * https://github.com/AnalyticalGraphicsInc/quantized-mesh
 *
 * @author Johann Sorel
 */
public class QMeshStore extends AbstractModel3DStore {

    public QMeshStore(Object input) {
        super(QMeshFormat.INSTANCE, input);
    }

    @Override
    public Collection getElements() throws StoreException {

        try {
            final DataInputStream ds = new DataInputStream(getSourceAsInputStream());
            ds.setEndianness(Endianness.LITTLE_ENDIAN);

            final Vector3f64 center = new Vector3f64(ds.readDouble(), ds.readDouble(), ds.readDouble());
            final float minHeight = ds.readFloat();
            final float maxHeight = ds.readFloat();
            final Vector3f64 bsCenter = new Vector3f64(ds.readDouble(), ds.readDouble(), ds.readDouble());
            final double bsRadius = ds.readDouble();
            final Vector3f64 occ = new Vector3f64(ds.readDouble(), ds.readDouble(), ds.readDouble());

            final int vertexCount = ds.readInt();
            final int[] zzu = ds.readUShort(vertexCount);
            final int[] zzv = ds.readUShort(vertexCount);
            final int[] zzh = ds.readUShort(vertexCount);
            final double[] uvz = new double[vertexCount*3];

            int u = 0;
            int v = 0;
            int h = 0;
            for (int i=0; i<vertexCount; i++) {
                u += zigzagDecode(zzu[i]);
                v += zigzagDecode(zzv[i]);
                h += zigzagDecode(zzh[i]);
                uvz[i*3+0] = u;
                uvz[i*3+1] = v;
                uvz[i*3+2] = h;
            }

            final int[] indices;
            if (vertexCount <= 65536) {
                final int triangleCount = ds.readInt();
                indices = ds.readUShort(triangleCount*3);
            } else {
                final int triangleCount = ds.readInt();
                indices = ds.readInt(triangleCount*3);
            }

            int highest = 0;
            for (int i = 0; i < indices.length; ++i) {
                int code = indices[i];
                indices[i] = highest - code;
                if (code == 0) {
                    ++highest;
                }
            }

            final int[] westIndices;
            final int[] southIndices;
            final int[] eastIndices;
            final int[] northIndices;
            if (vertexCount <= 65536) {
                westIndices = ds.readUShort(ds.readInt());
                southIndices = ds.readUShort(ds.readInt());
                eastIndices = ds.readUShort(ds.readInt());
                northIndices = ds.readUShort(ds.readInt());
            } else {
                westIndices = ds.readInt(ds.readInt());
                southIndices = ds.readInt(ds.readInt());
                eastIndices = ds.readInt(ds.readInt());
                northIndices = ds.readInt(ds.readInt());
            }

            final DefaultMesh geom = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
            geom.setPositions(new InterleavedTupleGrid1D(DefaultBufferFactory.wrap(uvz), Float64.TYPE, CoordinateSystems.UNDEFINED_3D, uvz.length));
            geom.setIndex(new IBO(indices));
            geom.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, indices.length)});

            final SimpleBlinnPhong.Material mat = SimpleBlinnPhong.newMaterial();
            mat.setDiffuse(Color.GRAY_NORMAL);

            final DefaultModel mesh = new DefaultModel();
            mesh.setShape(geom);
            mesh.getMaterials().add(mat);

            final Sequence seq = new ArraySequence();
            seq.add(mesh);
            return seq;

        } catch (IOException ex) {
            throw new StoreException(ex);
        }

    }

    private static int zigzagDecode(int value) {
        return (value >> 1) ^ (-(value & 1));
    }

}
