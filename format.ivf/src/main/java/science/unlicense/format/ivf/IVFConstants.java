

package science.unlicense.format.ivf;

import science.unlicense.common.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class IVFConstants {

    public static final Chars SIGNATURE = Chars.constant(new byte[]{'D','K','I','F'});

    private IVFConstants(){}

}
