

package science.unlicense.format.ivf;

import science.unlicense.common.api.exception.UnimplementedException;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.AbstractStore;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.MediaWriteParameters;
import science.unlicense.media.api.MediaWriteStream;
import science.unlicense.media.api.Track;

/**
 * IVF is a simple container for VP8 raw data.
 *
 * Specification :
 * http://wiki.multimedia.cx/index.php?title=IVF
 *
 * @author Johann Sorel
 */
public class IVFMediaStore extends AbstractStore implements Media {

    private final Path input;

    public IVFMediaStore(Path input) {
        super(IVFFormat.INSTANCE,input);
        this.input = input;
    }

    @Override
    public Track[] getTracks() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
