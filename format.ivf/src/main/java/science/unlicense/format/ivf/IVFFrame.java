

package science.unlicense.format.ivf;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.format.vp8.vp8.VP8Frame;

/**
 *
 * @author Johann Sorel
 */
public class IVFFrame implements IVFElement {

    /** 4 bytes : frame length in bytes */
    public int length;
    /** 8 bytes : timestamp */
    public long timeStamp;
    /** VP8 frame */
    public VP8Frame frame;

    public void read(DataInputStream ds) throws IOException {
        timeStamp = ds.readLong();
        frame = new VP8Frame();
        frame.read(ds);
    }

}
