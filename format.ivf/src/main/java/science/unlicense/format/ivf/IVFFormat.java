
package science.unlicense.format.ivf;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.media.api.AbstractMediaFormat;
import science.unlicense.media.api.Media;

/**
 *
 * @author Johann Sorel
 */
public class IVFFormat extends AbstractMediaFormat {

    public static final IVFFormat INSTANCE = new IVFFormat();

    public IVFFormat() {
        super(new Chars("ivf"));
        shortName = new Chars("IVF");
        longName = new Chars("IVF");
        extensions.add(new Chars("ivf"));
        signatures.add(new byte[]{'D','K','I','F'});
        resourceTypes.add(Media.class);
    }

    @Override
    public Store open(Object input) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
