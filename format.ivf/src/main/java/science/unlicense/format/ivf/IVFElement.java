
package science.unlicense.format.ivf;

import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public interface IVFElement {

    void read(DataInputStream ds) throws IOException;

}
